from configparser import ConfigParser

cfg = ConfigParser()
cfg.read("setup.cfg")
version = int(cfg["metadata"]["version"].split(".")[2])
cfg.set("metadata", "version", f"0.0.{version + 1}")
with open("setup.cfg", "w") as fp:
    cfg.write(fp)