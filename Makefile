.PHONY: docs

test-publish:
	rm -rf dist
	rm -rf build
	python3 inc_version.py
	python3 -m build
	python3 -m twine upload --repository testpypi dist/*