import unittest
from requests.models import Response
import toml
from src.kardia_api.kardia import Kardia


class TestCRM(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestCRM, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")
        self.partner_id = self.config["crm"]["partner_id"]
        self.tag = self.config["crm"]["tag"]
        self.contact_history_id = self.config["crm"]["contact_history_id"]
        self.track = self.config["crm"]["track"]
        self.step = self.config["crm"]["step"]
        self.track_id = self.config["crm"]["track_id"]
        self.collaborator_id = self.config["crm"]["collaborator_id"]
        self.step_id = self.config["crm"]["step_id"]
        self.tag_type_id = self.config["crm"]["tag_type_id"]
        self.contact_history_type_id = self.config["crm"]["contact_history_type_id"]
        self.collab_type_id = self.config["crm"]["collab_type_id"]
        self.todo_type_id = self.config["crm"]["todo_type_id"]
        self.supporter_id = self.config["crm"]["supporter_id"]
        self.missionary_id = self.config["crm"]["missionary_id"]

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def doTest(self, res: Response, val, js_key=None):
        self.assertEquals(res.status_code, 200, res.__dict__)
        try:
            js = res.json()
        except Exception as e:
            self.fail(e)
        if js_key:
            try:
                val = js[js_key]
            except Exception as e:
                self.fail("{}\n{}".format(e, js))
            self.assertEqual(js[js_key], val, res.__dict__)
        else:
            self.assertIn(str(val), js, res.__dict__)

    def testGetPartners(self):
        res = self.kardia.crm.getPartners()
        self.doTest(res, self.partner_id)

    def testGetPartner(self):
        res = self.kardia.crm.getPartner(self.partner_id)
        self.doTest(res, self.partner_id, "partner_id")

    def testGetPartnerTags(self):
        res = self.kardia.crm.getPartnerTags(self.partner_id)
        self.doTest(res, self.tag)

    def testGetPartnerTag(self):
        res = self.kardia.crm.getPartnerTag(self.partner_id, self.tag)
        self.doTest(res, self.tag, "name")

    def testGetParnterDocuments(self):
        res = self.kardia.crm.getPartnerDocuments(self.partner_id)
        self.doTest(res, "@id")

    def testGetPartnerDocument(self):
        self.fail("TODO")

    def testGetPartnerContactHistory(self):
        res = self.kardia.crm.getPartnerContactHistory(self.partner_id)
        self.doTest(res, self.contact_history_id)

    def testGetPartnerContactHistoryById(self):
        res = self.kardia.crm.getPartnerContactHistory(
            self.partner_id, self.contact_history_id)
        self.doTest(res, int(self.contact_history_id), "contact_history_id")

    def testGetPartnerContactAutoRecord(self):
        res = self.kardia.crm.getPartnerContactAutorecord(self.partner_id)
        self.doTest(res, "@id")

    def testGetPartnerContactAutoRecordById(self):
        self.fail("TODO")

    def testGetPartnerTracks(self):
        res = self.kardia.crm.getPartnerTracks(self.partner_id)
        self.doTest(res, "@id")

    def testGetPartnerTrack(self):
        self.fail("TODO")

    def testGetTracks(self):
        res = self.kardia.crm.getTracks()
        self.doTest(res, self.track)

    def testGetTrack(self):
        res = self.kardia.crm.getTrack(self.track)
        self.doTest(res, self.track, "name")

    def testGetTrackSteps(self):
        res = self.kardia.crm.getTrackSteps(self.track)
        self.doTest(res, self.step)

    def testGetTrackStep(self):
        res = self.kardia.crm.getTrackStep(self.track, self.step)
        self.doTest(res, self.step, "name")

    def testGetStepCollaborators(self):
        res = self.kardia.crm.getStepCollaborators(self.track, self.step)
        self.doTest(res, "{}|{}|{}".format(self.track_id,
                    self.step_id, self.collaborator_id))

    def testGetStepCollaborator(self):
        res = self.kardia.crm.getStepCollaborator(
            self.track, self.step, self.track_id, self.step_id, self.collaborator_id)
        self.doTest(res, self.partner_id, "partner_id")

    def testGetTrackCollaborators(self):
        res = self.kardia.crm.getTrackCollaborators(self.track)
        self.doTest(res, "{}|{}".format(self.track_id, self.collaborator_id))

    def testGetTrackCollaborator(self):
        res = self.kardia.crm.getTrackCollaborator(
            self.track, self.track_id, self.collaborator_id)
        self.doTest(res, "{}|{}".format(
            self.track_id, self.collaborator_id), "name")

    def testGetTagTypes(self):
        res = self.kardia.crm.getTagTypes()
        self.doTest(res, self.tag_type_id)

    def testGetTagType(self):
        res = self.kardia.crm.getTagType(self.tag_type_id)
        self.doTest(res, self.tag_type_id, "tag_id")
        print(res.json())

    def testGetContactHistoryTypes(self):
        res = self.kardia.crm.getContactHistTypes()
        self.doTest(res, self.contact_history_type_id)

    def testGetContactHistoryType(self):
        res = self.kardia.crm.getContactHistType(1)
        self.doTest(res, self.contact_history_type_id, "id")

    def testGetCollaboratorTypes(self):
        res = self.kardia.crm.getCollaboratorTypes()
        self.doTest(res, self.collab_type_id)

    def testGetCollaboratorType(self):
        res = self.kardia.crm.getCollaboratorType(self.collab_type_id)
        self.doTest(res, self.collab_type_id, "id")

    def testGetTodoTypes(self):
        res = self.kardia.crm.getTodoTypes()
        self.doTest(res, self.todo_type_id)

    def testGetTodoType(self):
        res = self.kardia.crm.getTodoType(self.todo_type_id)
        self.doTest(res, self.todo_type_id, "type_id")

    def testGetWorkflowTypes(self):
        res = self.kardia.crm.getWorkflowTypes()
        self.doTest(res, "@id")
        self.fail("TODO")

    def testGetWorkflowType(self):
        self.fail("TODO")

    def testGetDataItemTypes(self):
        res = self.kardia.crm.getDataItemTypes()
        self.doTest(res, "@id")

    def testGetDataItemType(self):
        self.fail("TODO")

    def testGetDocumentTypes(self):
        res = self.kardia.crm.getDocumentTypes()
        self.doTest(res, "@id")

    def testGetDocumentType(self):
        self.fail("TODO")

    def testGetCountries(self):
        res = self.kardia.crm.getCountries()
        self.doTest(res, "United States")

    def testGetCountry(self):
        res = self.kardia.crm.getCountry("United States")
        self.fail("Broken in the API")
        self.doTest(res, "United States", "name")

    def testGetTextExpansions(self):
        res = self.kardia.crm.getTextExpansions()
        self.doTest(res, "@id")

    def testGetTextExpansion(self):
        self.fail("TODO")

    def testGetSupporters(self):
        res = self.kardia.crm.getSupporters()
        self.doTest(res, self.supporter_id)

    def testGetSupporter(self):
        res = self.kardia.crm.getSupporter(self.supporter_id)
        self.doTest(res, self.supporter_id, "partner_id")

    def testGetSupporterPrayers(self):
        res = self.kardia.crm.getSupporterPrayers(self.supporter_id)
        self.doTest(res, "@id")

    def testGetSupporterPrayer(self):
        res = self.kardia.crm.getSupporterPrayer(self.supporter_id, 1)
        self.fail("TODO")

    def testGetSupporterComments(self):
        res = self.kardia.crm.getSupporterComments(self.supporter_id)
        self.doTest(res, "@id")

    def testGetSupporteComment(self):
        res = self.kardia.crm.getSupporterComment(self.supporter_id, 1)
        self.doTest(res, self.supporter_id, "partner_id")

    def testGetSupportersMissionaries(self):
        res = self.kardia.crm.getSupporterMissionaries(self.supporter_id)
        self.doTest(res, self.supporter_id)

    def testGetSupporterMissionary(self):
        res = self.kardia.crm.getSupporterMissionary(self.supporter_id, 1)
        self.fail("TODO")

    def testGetMissionaries(self):
        res = self.kardia.crm.getMissionaries()
        self.doTest(res, self.missionary_id)

    def testGetMissionary(self):
        res = self.kardia.crm.getMissionary(self.missionary_id)
        self.doTest(res, self.missionary_id, "partner_id")
