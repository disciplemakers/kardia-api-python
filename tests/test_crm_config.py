from src.kardia_api.objects.crm_config_objects import Step, Track
import random
import string
import unittest
from requests.models import Response
import toml
import inspect
from src.kardia_api.kardia import Kardia


class TestCRM_Config(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestCRM_Config, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def doTest(self, res: Response, val, js_key=None):
        self.assertEquals(res.status_code, 200, res.__dict__)
        try:
            js = res.json()
        except Exception as e:
            self.fail(e)
        if js_key:
            try:
                val = js[js_key]
            except Exception as e:
                self.fail("{}\n{}".format(e, js))
            self.assertEqual(js[js_key], val, res.__dict__)
        else:
            self.assertIn(str(val), js, res.__dict__)

    @property
    def func(self):
        func = (lambda s: s[5:])(inspect.stack()[1][3])
        return getattr(self.kardia.crm_config, func)

    def test_getCollaboratorType(self):
        # TODO
        self.doTest(self.func(self.collab_type_id), val="TODO", js_key="TODO")

    def test_getCollaboratorTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getContactHistType(self):
        # TODO
        self.doTest(self.func(self.ch_type_id), val="TODO", js_key="TODO")

    def test_getContactHistTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getCountries(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getCountry(self):
        # TODO
        self.doTest(self.func(self.country), val="TODO", js_key="TODO")

    def test_getDataItemType(self):
        # TODO
        self.doTest(self.func(self.di_type_id), val="TODO", js_key="TODO")

    def test_getDataItemTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getDocumentType(self):
        # TODO
        self.doTest(self.func(self.doc_type_id), val="TODO", js_key="TODO")

    def test_getDocumentTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getStepCollaborator(self):
        # TODO
        self.doTest(self.func(self.track_name, self.step_name,
                    self.collab_id), val="TODO", js_key="TODO")

    def test_getStepCollaborators(self):
        # TODO
        self.doTest(self.func(self.track_name, self.step_name),
                    val="TODO", js_key="TODO")

    def test_getTagType(self):
        # TODO
        self.doTest(self.func(self.tag_id), val="TODO", js_key="TODO")

    def test_getTagTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getTextExpansion(self):
        # TODO
        self.doTest(self.func(self.expansion), val="TODO", js_key="TODO")

    def test_getTextExpansions(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getTodoType(self):
        # TODO
        self.doTest(self.func(self.todo_type_id), val="TODO", js_key="TODO")

    def test_getTodoTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getTrack(self):
        # TODO
        self.doTest(self.func(self.track_name), val="TODO", js_key="TODO")

    def test_getTrackCollaborator(self):
        # TODO
        self.doTest(self.func(self.track_name, self.step_name),
                    val="TODO", js_key="TODO")

    def test_getTrackCollaborators(self):
        # TODO
        self.doTest(self.func(self.track_name), val="TODO", js_key="TODO")

    def test_getTrackStep(self):
        # TODO
        self.doTest(self.func(self.track_name, self.step_name),
                    val="TODO", js_key="TODO")

    def test_getTrackSteps(self):
        # TODO
        self.doTest(self.func(self.track_name), val="TODO", js_key="TODO")

    def test_getTracks(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_getWorkflowType(self):
        # TODO
        self.doTest(self.func(self.wf_type_id), val="TODO", js_key="TODO")

    def test_getWorkflowTypes(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_createStepCollaborator(self):
        # TODO
        self.doTest(self.func(), val="TODO", js_key="TODO")

    def test_createTrack(self):
        rand = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        track = Track("Track {}".format(rand), "Track {} desc".format(
            rand), Track.Statuses.ACTIVE)
        self.doTest(self.func(track), val="TODO", js_key="TODO")

    def test_createTrackStep(self):
        rand = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        step = Step(1, "Step {}".format(rand), "Step {} desc".format(rand))
        self.doTest(self.func("Test Track", step), val="TODO", js_key="TODO")

    def test_createSetRequirement(self):
        # TODO
        self.doTest(self.func(self.track_name, self.step_name, self.req), val="TODO", js_key="TODO")
