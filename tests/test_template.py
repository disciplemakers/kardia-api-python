import unittest
from requests.models import Response
import toml
import inspect
from src.kardia_api.kardia import Kardia


class TestClass(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestClass, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def doTest(self, res: Response, val, js_key=None):
        self.assertEquals(res.status_code, 200, res.__dict__)
        try:
            js = res.json()
        except Exception as e:
            self.fail(e)
        if js_key:
            try:
                val = js[js_key]
            except Exception as e:
                self.fail("{}\n{}".format(e, js))
            self.assertEqual(js[js_key], val, res.__dict__)
        else:
            self.assertIn(str(val), js, res.__dict__)

    @property
    def func(self):
        func = (lambda s: s[5:])(inspect.stack()[1][3])
        return getattr(self.kardia.crm_config, func)
