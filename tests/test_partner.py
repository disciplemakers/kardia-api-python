import unittest
from requests.models import Response
import toml
import inspect
from src.kardia_api.kardia import Kardia


class TestPartner(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestPartner, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")
        self.partner_id = self.config["partner"]["partner_id"]
        self.mail_list_id = self.config["partner"]["mail_list_id"]
        self.staff_id = self.config["partner"]["staff_id"]
        self.username = self.config["partner"]["username"]

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def doTest(self, res: Response, val, js_key=None):
        self.assertEquals(res.status_code, 200, res.__dict__)
        try:
            js = res.json()
        except Exception as e:
            self.fail(res.__dict__)
        if js_key:
            try:
                val = js[js_key]
            except Exception as e:
                self.fail("{}\n{}".format(e, js))
            self.assertEqual(js[js_key], val, res.__dict__)
        else:
            self.assertIn(str(val), js, res.__dict__)

    def testGetPartners(self):
        res = self.kardia.partner.getPartners()
        self.doTest(res, self.partner_id)

    def testGetPartner(self):
        res = self.kardia.partner.getPartner(self.partner_id)
        self.doTest(res, self.partner_id, "partner_id")

    @property
    def func(self):
        func = (lambda s: s[4].lower() + s[5:])(inspect.stack()[1][3])
        return self.kardia.partner.__getattribute__(func)

    def testGetPartnerAddresses(self):
        self.doTest(self.func(self.partner_id),
                    "{}|1|0".format(self.partner_id))

    def testGetPartnerAddress(self):
        self.doTest(self.func(self.partner_id, 1),
                    "{}|1|0".format(self.partner_id), "name")

    def testGetPartnerContactInfos(self):
        self.doTest(self.func(self.partner_id),
                    "{}|1".format(self.partner_id))

    def testGetPartnerContactInfo(self):
        self.doTest(self.func(self.partner_id, 1),
                    "{}|1|0".format(self.partner_id), "name")

    def testGetPartnerSubscriptions(self):
        self.doTest(self.func(self.partner_id),
                    "{}|{}|1".format(self.mail_list_id, self.partner_id))

    def testGetPartnerSubscription(self):
        self.doTest(self.func(self.partner_id, self.mail_list_id),
                    "{}|{}|1".format(self.mail_list_id, self.partner_id), "name")

    def testGetStaff(self):
        self.doTest(self.func(), self.staff_id)

    def testGetStaffMember(self):
        self.doTest(self.func(self.staff_id), self.staff_id, "partner_id")

    def testGetStaffLogins(self):
        self.doTest(self.func(), self.username)

    def testGetStaffLogin(self):
        self.doTest(self.func(self.username), "@id")

    def testGetContactTypes(self):
        self.doTest(self.func(), "Blog")

    def testGetContactType(self):
        self.doTest(self.func("Blog"), "@id")

    def testGetTests(self):
        self.doTest(self.func(), "Testing1")

    def testGetTest(self):
        self.doTest(self.func("Testing1"), "@id")

    def testGetNextPartnerKey(self):
        self.doTest(self.func(), "NextPartnerKey", "name")
