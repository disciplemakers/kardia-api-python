from src.kardia_api.objects.gl_objects import Account, Batch, Fund, Period, Transaction, Year
import unittest
import toml
import random
import string
import requests
import json
from datetime import datetime, timedelta
import random


class TestGL(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestGL, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")
        self.ledger_id = self.config["gl"]["ledger_id"]
        self.year_id = self.config["gl"]["year_id"]
        self.period_id = self.config["gl"]["period_id"]
        self.batch_id = self.config["gl"]["batch_id"]
        self.journal_id = self.config["gl"]["journal_id"]
        self.trans_id = self.config["gl"]["trans_id"]
        self.account_code = self.config["gl"]["account_code"]
        self.fund = self.config["gl"]["fund"]
        self.subfund = self.config["gl"]["subfund"]

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def formatResponse(self, res: requests.Response):
        formatted = "-- URL --"
        formatted += "\n<{}> {}".format(res.status_code,
                                        requests.utils.unquote(res.url))
        if res.request.method == "POST" or res.request.method == "PUT":
            formatted += "\n\n-- {} Body --".format(res.request.method)
            if res.request.body:
                body = json.loads(res.request.body)
                formatted += "\n" + \
                    json.dumps(body, sort_keys=True, indent=4,
                               separators=(',', ': '))
        formatted += "\n\n-- Response Body --"
        try:
            formatted += "\n" + \
                json.dumps(res.json(), sort_keys=True,
                           indent=4, separators=(',', ': '))
        except:
            formatted += "\n" + str(res.content)
            formatted += "\n\n-- Response Headers --"
            formatted += "\n" + \
                json.dumps(res.headers.__dict__, sort_keys=True,
                           indent=4, separators=(',', ': '))
            formatted += "\n\n-- Request Headers --"
            formatted += "\n" + \
                json.dumps(res.request.headers.__dict__,
                           sort_keys=True, indent=4, separators=(',', ': '))
        formatted += "\n"
        return formatted

    def test_getAccounts(self):
        res = self.kardia.gl.getAccounts(self.ledger_id)
        self.assertTrue(res.status_code == 200, self.formatResponse(res))
        try:
            self.assertTrue("{}|{}".format(
                self.account_code, self.ledger_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getAccount(self):
        res = self.kardia.gl.getAccount(self.ledger_id, self.account_code)
        self.assertTrue(res.status_code == 200, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_account_code"] == self.account_code)
        except Exception as e:
            self.fail(e)

    def test_createAccount(self):
        acc_code = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        acc = Account(acc_code, self.ledger_id, "Test account.")
        res = self.kardia.gl.createAccount(acc)
        self.assertTrue(res.status_code == 201, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_account_code"]["v"] == acc_code)
        except Exception as e:
            self.fail(e)

    def test_getFunds(self):
        res = self.kardia.gl.getFunds(self.ledger_id)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue("{}|{}".format(
                self.fund, self.ledger_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getFund(self):
        res = self.kardia.gl.getFund(self.ledger_id, self.fund)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue(res.json()["a_fund"] == self.fund)
        except Exception as e:
            self.fail(e)

    def test_createFund(self):
        fund_id = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        fund = Fund(fund_id, self.ledger_id)
        res = self.kardia.gl.createFund(fund)
        self.assertTrue(res.status_code == 201, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_fund"]["v"] == fund_id)
        except Exception as e:
            self.fail(e)

    def test_getSubfunds(self):
        res = self.kardia.gl.getSubfunds(self.ledger_id, self.fund)
        self.assertTrue(res.status_code == 200, res)
        try:
            self.assertTrue("{}|{}".format(
                self.subfund, self.ledger_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getSubfund(self):
        res = self.kardia.gl.getSubfund(
            self.ledger_id, self.fund, self.subfund)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue(res.json()["a_fund"] == self.subfund)
        except Exception as e:
            self.fail(e)

    def test_createSubfund(self):
        self.fail("TODO")

    def test_getYears(self):
        res = self.kardia.gl.getYears(self.ledger_id)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue("{}|{}".format(
                self.year_id, self.ledger_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getYear(self):
        res = self.kardia.gl.getYear(self.ledger_id, self.year_id)
        self.assertTrue(res.status_code == 200)
        try:
            print(res.json())
            self.assertTrue(res.json()["a_period"] == self.year_id)
        except Exception as e:
            self.fail(e)

    def test_createYear(self):
        year_id = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=7))
        start = datetime.today()
        end = start + timedelta(days=365)
        desc = "Test year {}".format(year_id)
        year = Year(year_id, self.ledger_id, start, end, desc)
        res = self.kardia.gl.createYear(year)
        self.assertTrue(res.status_code == 201, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_period"]["v"] == year_id)
        except Exception as e:
            self.fail(e)

    def test_createMonth(self):
        month_id = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=7))
        res = self.kardia.gl.createMonth(
            self.ledger_id, self.year_id, random.randint(1, 12), month_id)
        self.assertTrue(res.status_code == 201, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_period"]["v"] == month_id)
        except Exception as e:
            self.fail(e)

    def test_getPeriods(self):
        res = self.kardia.gl.getPeriods(self.ledger_id, self.year_id)
        self.assertTrue(res.status_code == 200, self.formatResponse(res))
        try:
            print(res.json())
            self.assertTrue("{}|{}".format(
                self.period_id, self.ledger_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getPeriod(self):
        res = self.kardia.gl.getPeriod(
            self.ledger_id, self.year_id, self.period_id)
        self.assertTrue(res.status_code == 200, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_period"] == self.period_id)
        except Exception as e:
            self.fail(e)

    def test_createPeriod(self, parent_id=None, summary=True):
        period_id = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=7))
        p = Period(period_id, self.ledger_id, datetime.today(
        ), datetime.today() + timedelta(days=1), parent_id, summary=summary)
        res = self.kardia.gl.createPeriod(p)
        self.assertTrue(res.status_code == 201, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_period"]["v"] == period_id)
        except Exception as e:
            self.fail(e)
        return res

    def test_createSubPeriod(self):
        parent_id = None
        for i in range(2):
            res = self.test_createPeriod(parent_id, i == 0)
            parent_id = res.json()["a_period"]["v"]

    def test_getBatches(self):
        res = self.kardia.gl.getBatches(
            self.ledger_id, self.year_id, self.period_id)
        self.assertTrue(res.status_code == 200, self.formatResponse(res))
        try:
            self.assertTrue("{}|{}".format(
                self.batch_id, self.ledger_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getBatch(self):
        res = self.kardia.gl.getBatch(
            self.ledger_id, self.year_id, self.period_id, self.batch_id)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue(res.json()["a_batch_number"] == self.batch_id)
        except Exception as e:
            self.fail(e)

    def test_getNextBatchId(self):
        res = self.kardia.gl.getNextBatchId(self.ledger_id)
        # self.assertTrue(res.status_code == 200)
        try:
            batch_id_1 = res
        except Exception as e:
            self.fail("{}\n{}".format(e, self.formatResponse(res)))
        res = self.kardia.gl.getNextBatchId(self.ledger_id)
        # self.assertTrue(res.status_code == 200)
        try:
            batch_id_2 = res
        except Exception as e:
            self.fail(e)
        self.assertTrue(batch_id_2 - batch_id_1 == 1)

    def test_createBatch(self):
        desc = "Test batch: {}".format(
            "".join(random.choices(string.ascii_uppercase + string.digits, k=10)))
        batch = Batch(self.ledger_id, self.period_id, desc=desc)
        res = self.kardia.gl.createBatch(self.year_id, batch)
        self.assertTrue(res.status_code == 201)
        try:
            self.assertTrue(res.json()["a_batch_desc"]["v"] == desc)
        except Exception as e:
            self.fail(e)

    def test_getTransactions(self):
        res = self.kardia.gl.getTransactions(
            self.ledger_id, self.year_id, self.period_id, self.batch_id)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue("{}|{}|{}|{}".format(
                self.ledger_id, self.batch_id, self.journal_id, self.trans_id) in res.json())
        except Exception as e:
            self.fail(e)

    def test_getTransaction(self):
        res = self.kardia.gl.getTransaction(
            self.ledger_id, self.year_id, self.period_id, self.batch_id, self.journal_id, self.trans_id)
        self.assertTrue(res.status_code == 200)
        try:
            self.assertTrue(res.json()["a_batch_number"] == self.batch_id)
            self.assertTrue(res.json()["a_journal_number"] == self.journal_id)
            self.assertTrue(
                res.json()["a_transaction_number"] == self.trans_id)
        except Exception as e:
            self.fail(e)

    def test_createTransaction(self):
        comment = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        t = Transaction(self.ledger_id, self.period_id, self.batch_id, 1,
                        fund=self.fund, account_code=self.account_code, amount="20.00", comment=comment)
        res = self.kardia.gl.createTransaction(self.year_id, t)
        self.assertTrue(res.status_code == 201, self.formatResponse(res))
        try:
            self.assertTrue(res.json()["a_comment"]["v"] == comment)
        except Exception as e:
            self.fail(e)

    def test_createTransactions(self):
        comment1 = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        t1 = Transaction(self.ledger_id, self.period_id, self.batch_id, 1,
                         fund=self.fund, account_code=self.account_code, amount="20.00", comment=comment1)
        comment2 = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        t2 = Transaction(self.ledger_id, self.period_id, self.batch_id, 1,
                         fund=self.fund, account_code=self.account_code, amount="25.00", comment=comment2)
        responses = self.kardia.gl.createTransactions(self.year_id, [t1, t2])
        for res in responses:
            self.assertTrue(res.status_code == 201)
            try:
                a_comment = res.json()["a_comment"]["v"]
                self.assertTrue(a_comment == comment1 or a_comment == comment2)
            except Exception as e:
                self.fail(e)

    def test_createJEWithTrans(self):
        comment1 = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        t1 = Transaction(
            fund=self.fund, account_code=self.account_code, amount="100.10", comment=comment1)
        comment2 = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        t2 = Transaction(
            fund=self.fund, account_code=self.account_code, amount="200.94", comment=comment2)
        desc = "".join(random.choices(
            string.ascii_uppercase + string.digits, k=10))
        res = self.kardia.gl.createJEWithTrans(
            self.ledger_id, self.year_id, self.period_id, [t1, t2], desc=desc)
        batch_res, trans_res = res["batch"], res["transactions"]
        self.assertTrue(batch_res.status_code == 201)
        try:
            self.assertTrue(batch_res.json()["a_batch_desc"]["v"] == desc)
        except Exception as e:
            self.fail(e)
        for res in trans_res:
            self.assertTrue(res.status_code == 201)
            try:
                a_comment = res.json()["a_comment"]["v"]
                self.assertTrue(a_comment == comment1 or a_comment == comment2)
            except Exception as e:
                self.fail(e)


if __name__ == "__main__":
    unittest.main()
