import toml
import unittest
from src.kardia_api.kardia import Kardia

class TestFiles(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.config = toml.load("tests/config.toml")
        self.kardia = Kardia(self.config["kardia_url"], self.config["user"], self.config["pw"])

    def testGetFile(self):
        res = self.kardia.files.getFile("fundgift_template.txt")
        self.assertTrue(res.url.endswith("apps/kardia/files/fundgift_template.txt?cx__mode=rest"))
