import unittest
import toml
from src.kardia_api.modules.api_module import APIModule


class TestAPIModule(unittest.TestCase):

    def setUp(self):
        self.config = toml.load("tests/config.toml")
        keys = ["kardia_url", "user", "pw"]
        self.api = APIModule(**{key: self.config[key] for key in keys})

    def testInit(self):
        self.assertTrue(self.api != None)

    def testSetAkey(self):
        try:
            self.api.setAkey()
        except Exception as e:
            self.fail(e)
        self.assertTrue(isinstance(self.api.akey, str))

    def testSetParams(self):
        self.api.setParams(mode="rest", res_type="both",
                           res_format="both", res_attrs="full", res_levels=2)
        self.assertEqual(self.api.mode, "rest")
        self.assertEqual(self.api.res_type, "both")
        self.assertEqual(self.api.res_format, "both")
        self.assertEqual(self.api.res_attrs, "full")
        self.assertEqual(self.api.res_levels, 2)

    def testParams(self):
        self.api.setAkey()
        self.api.setParams(mode="rest", res_type="both",
                           res_format="both", res_attrs="full", res_levels=2)
        self.api.method = "post"
        params = self.api.params
        self.assertEqual(params["cx__mode"], "rest")
        self.assertEqual(params["cx__res_type"], "both")
        self.assertEqual(params["cx__res_format"], "both")
        self.assertEqual(params["cx__res_attrs"], "full")
        self.assertEqual(params["cx__res_levels"], 2)
        self.assertEqual(params["cx__akey"], self.api.akey)

    def checkDefaultParams(self):
        self.assertEqual(self.api.mode, "rest")
        self.assertEqual(self.api.res_type, None)
        self.assertEqual(self.api.res_format, None)
        self.assertEqual(self.api.res_attrs, None)
        self.assertEqual(self.api.res_levels, None)

    def testResetParams(self):
        self.api.resetParams()
        self.checkDefaultParams()

    def testReset(self):
        self.api.reset()
        self.checkDefaultParams()
        self.assertEqual(self.api.endpoint, "")
        self.assertEqual(self.api.method, "")
        self.assertEqual(self.api.payload, None)

    def testRequest(self):
        try:
            res = self.api.request("get", "/modules", res_type="collection")
            payload = res.json()
            self.assertIn("gl", payload)
        except Exception as e:
            self.fail(e)

    def testGet(self):
        try:
            res = self.api.get("/modules", res_type="collection")
            payload = res.json()
            self.assertIn("gl", payload)
        except Exception as e:
            self.fail(e)

    def testPost(self):
        self.fail("TODO")
