import unittest
from requests.models import Response
import toml
import inspect
from src.kardia_api.kardia import Kardia


class TestDonor(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestDonor, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")
        self.partner_id = self.config["donor"]["partner_id"]

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def doTest(self, res: Response, val, js_key=None):
        self.assertEquals(res.status_code, 200, res.__dict__)
        try:
            js = res.json()
        except Exception as e:
            self.fail(e)
        if js_key:
            try:
                val = js[js_key]
            except Exception as e:
                self.fail("{}\n{}".format(e, js))
            self.assertEqual(js[js_key], val, res.__dict__)
        else:
            self.assertIn(str(val), js, res.__dict__)

    @property
    def func(self):
        func = (lambda s: s[5:])(inspect.stack()[1][3])
        return getattr(self.kardia.donor, func)

    def test_getDonor(self):
        # TODO
        self.doTest(self.func(self.partner_id),
                    val=self.partner_id, js_key="p_partner_key")

    def test_getDonors(self):
        # TODO
        self.doTest(self.func(), val=self.partner_id)

    def test_getDonorGift(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.gift_number),
                    val="TODO", js_key="TODO")

    def test_getDonorGifts(self):
        # TODO
        self.doTest(self.func(self.partner_id), val="TODO", js_key="TODO")

    def test_getGiftReceipt(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.gift_number),
                    val="TODO", js_key="TODO")

    def test_getYear(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id),
                    val="TODO", js_key="TODO")

    def test_getYears(self):
        # TODO
        self.doTest(self.func(self.partner_id), val="TODO", js_key="TODO")

    def test_getYearGift(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id,
                    self.gift_number), val="TODO", js_key="TODO")

    def test_getYearGifts(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id),
                    val="TODO", js_key="TODO")

    def test_getYearGiftReceipt(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id,
                    self.gift_number), val="TODO", js_key="TODO")

    def test_getYearFund(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id,
                    self.fund_id), val="TODO", js_key="TODO")

    def test_getYearFunds(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id),
                    val="TODO", js_key="TODO")

    def test_getYearFundGift(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id,
                    self.fund_id, self.gift_number), val="TODO", js_key="TODO")

    def test_getYearFundGifts(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id,
                    self.fund_id), val="TODO", js_key="TODO")

    def test_getFund(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id),
                    val="TODO", js_key="TODO")

    def test_getFunds(self):
        # TODO
        self.doTest(self.func(self.partner_id), val="TODO", js_key="TODO")

    def test_getFundGift(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.gift_number), val="TODO", js_key="TODO")

    def test_getFundGiftReceipt(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.gift_number), val="TODO", js_key="TODO")

    def test_getFundGifts(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id),
                    val="TODO", js_key="TODO")

    def test_getFundYear(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.year_id, self.gift_number), val="TODO", js_key="TODO")

    def test_getFundYears(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.year_id), val="TODO", js_key="TODO")

    def test_getFundYearGift(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.year_id, self.gift_number), val="TODO", js_key="TODO")

    def test_getFundYearGifts(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.year_id), val="TODO", js_key="TODO")

    def test_getFundYearGiftReceipt(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.fund_id,
                    self.year_id, self.gift_number), val="TODO", js_key="TODO")

    def test_getYearFundGiftReceipt(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id,
                    self.fund_id, self.gift_number), val="TODO", js_key="TODO")

    def test_getDonorInfo(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id),
                    val="TODO", js_key="TODO")

    def test_getGivingInfo(self):
        # TODO
        self.doTest(self.func(self.partner_id, self.year_id),
                    val="TODO", js_key="TODO")
