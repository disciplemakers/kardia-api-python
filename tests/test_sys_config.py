import unittest
import toml
from src.kardia_api.kardia import Kardia


class TestConfig(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestConfig, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")
        self.ledger = self.config["config"]["ledger"]

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"])

    def test_getConfig(self):
        res = self.kardia.sys_config.getConfig(self.ledger)
        self.assertEqual(res.status_code, 200, res.__dict__)
        try:
            payload = res.json()
        except Exception:
            self.fail(res.content)
        self.assertEqual(payload["a_ledger_number"], self.ledger)
