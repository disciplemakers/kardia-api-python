import responses
import unittest
from enum import Enum
from src.kardia_api.modules.api_module import APIModule

class TestAPIModuleWMocks(unittest.TestCase):


    class MockEndpoints(Enum):
        PARENT = None
        PATCH = "patch"


    def setUp(self):
        self.apiModule = APIModule("http://localhost/apps/kardia", "user", "password")


    @responses.activate
    def test_patch(self):
        akey_string = "akey_string"
        responses.add(responses.GET, "http://localhost/apps/kardia/api/",
            json={"akey": akey_string})
        responses.add(responses.PATCH, "http://localhost/apps/kardia/api/example_module/patch/object_name")

        self.apiModule.module = "example_module"
        self.apiModule.payload = {"example": "payload"}
        self.apiModule._patchEndpoint(self.MockEndpoints.PATCH, "object_name")

        akey_request = responses.calls[0].request
        self.assertEqual(akey_request.method, responses.GET)
        patch_request = responses.calls[1].request
        self.assertEqual(patch_request.method, responses.PATCH)
        self.assertEqual(patch_request.params["cx__akey"], akey_string)
        self.assertEqual(patch_request.body, b'{"example": "payload"}')
