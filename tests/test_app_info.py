import toml
import unittest
from src.kardia_api.kardia import Kardia

class TestAppInfo(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.config = toml.load("tests/config.toml")
        self.kardia = Kardia(self.config["kardia_url"], self.config["user"], self.config["pw"])

    def testGetAppInfo(self):
        res = self.kardia.app_info.getAppInfo()
        json = res.json()
        self.assertIn("app_name", json)
        self.assertIn("app_version", json)
        self.assertIn("app_release", json)
