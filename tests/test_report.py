import unittest
from unittest import mock
from requests.models import Response
import toml
from datetime import datetime
from src.kardia_api.kardia import Kardia
from src.kardia_api.objects.report_objects import SchedReportBatchStatus, SchedReportStatus, SchedStatusTypes

# Be forewarned: these unit tests are structured differently from the rest of the tests in the project!
class TestReport(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.config = toml.load("tests/config.toml")
        self.kardia = Kardia(self.config["kardia_url"], self.config["user"], self.config["pw"])

    def testGetReport(self):
        res = self.kardia.report.getReport(self.config["report"]["report_file"], {
            "document_format": "application/pdf"
        })
        # One attribute that should be present on a report json
        assert("cx__objcontent" in res.json())

    def testGetSchedReportsToBeSent(self):
        res = self.kardia.report.getSchedReportsToBeSent()
        assert(self.config["report"]["sched_report_name"] in res.json())

    def testGetSchedReportsToBeSentWithAttrs(self):
        self.kardia.report.setParams(res_attrs="basic")
        res = self.kardia.report.getSchedReportsToBeSent()
        sched_report_info = res.json()[self.config["report"]["sched_report_name"]]
        # One attribute that should be present on a scheduled report
        assert("report_file" in sched_report_info)

    def testGetSchedReportParams(self):
        res = self.kardia.report.getSchedReportParams(self.config["report"]["sched_report_name"])
        for sched_report_param in self.config["report"]["sched_report_params"]:
            assert(sched_report_param in res.json())

    def testGetSchedReportParamsWithAttrs(self):
        self.kardia.report.setParams(res_attrs="basic")
        res = self.kardia.report.getSchedReportParams(self.config["report"]["sched_report_name"])
        sched_report_param_info = res.json()[self.config["report"]["sched_report_params"][0]]
        # One attribute that should be present on a scheduled report parameter
        assert("param_value" in sched_report_param_info)

    def testGetSchedReportBatches(self):
        res = self.kardia.report.getSchedReportBatches()
        assert(self.config["report"]["sched_report_batch_name"] in res.json())

    def testGetSchedReportBatchesWithAttrs(self):
        self.kardia.report.setParams(res_attrs="basic")
        res = self.kardia.report.getSchedReportBatches()
        sched_report_batch_info = res.json()[self.config["report"]["sched_report_batch_name"]]
        # One attribute that should be present on a scheduled report batch
        assert("sent_status" in sched_report_batch_info)

    @mock.patch("requests.Session.patch")
    def testUpdateSchedReportStatus(self, mock_patch):
        sched_report_status = SchedReportStatus(
            SchedStatusTypes("T"),
            "sent error",
            datetime.now(),
            "report path"
        )
        sched_report_name = self.config["report"]["sched_report_name"]
        self.kardia.report.updateSchedReportStatus(sched_report_name, sched_report_status)

        mock_patch.assert_called_once()
        (args, kwargs) = mock_patch.call_args
        assert(args[0].endswith(f"/apps/kardia/api/report/ScheduledReportsToBeSent/{sched_report_name}"))
        json = kwargs["json"]
        assert(json["sent_status"] == "T")
        assert(json["sent_error"] == "sent error")
        assert(json["generated_report_path"] == "report path")
        sent_date = json["sent_date"]
        assert("year" in sent_date and "month" in sent_date and "day" in sent_date and "hour" in sent_date and
            "minute" in sent_date and "second" in sent_date)

    @mock.patch("requests.Session.patch")
    def testUpdateSchedReportBatchStatus(self, mock_patch):
        sched_report_batch_status = SchedReportBatchStatus(sent_status = SchedStatusTypes("T"))
        sched_report_batch_name = self.config["report"]["sched_report_batch_name"]
        self.kardia.report.updateSchedReportBatchStatus(sched_report_batch_name, sched_report_batch_status)

        mock_patch.assert_called_once()
        (args, kwargs) = mock_patch.call_args
        assert(args[0].endswith(f"/apps/kardia/api/report/ScheduledReportBatches/{sched_report_batch_name}"))
        assert(kwargs["json"] == {"sent_status": "T"})

