import unittest
import toml
from src.kardia_api.kardia import Kardia

class TestConfig(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.config = toml.load("tests/config.toml")
        self.kardia = Kardia(self.config["kardia_url"], self.config["user"], self.config["pw"])

    def testGetConfig(self):
        res = self.kardia.config.getConfig()
        # one config value that should be present
        assert(self.config["config"]["config_attr_name"] in res.json())

    def testGetConfigWithAttrs(self):
        self.kardia.config.setParams(res_attrs="basic")
        res = self.kardia.config.getConfig()
        config_info = res.json()[self.config["config"]["config_attr_name"]]
        assert("s_config_name" in config_info)
        assert("s_config_value" in config_info)
