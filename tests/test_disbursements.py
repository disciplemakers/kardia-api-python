from datetime import datetime
from src.kardia_api.objects.disb_objects import LineItem
import unittest
from requests.models import Response
import toml
import inspect
from src.kardia_api.kardia import Kardia
from decimal import Decimal


class TestDisbursements(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestDisbursements, self).__init__(*args, **kwargs)
        self.config = toml.load("tests/config.toml")
        self.ledger = self.config["disb"]["ledger_id"]
        self.year = self.config["disb"]["year_id"]
        self.month = self.config["disb"]["period_id"]
        self.batch = self.config["disb"]["batch_id"]
        self.check = self.config["disb"]["check_id"]
        self.line_item = self.config["disb"]["line_item_id"]

    def setUp(self):
        self.kardia = Kardia(
            self.config["kardia_url"], self.config["user"], self.config["pw"]
        )

    def doTest(self, res: Response, val, js_key=None):
        self.assertEquals(res.status_code, 200, res.__dict__)
        try:
            js = res.json()
        except Exception as e:
            self.fail(e)
        if js_key:
            try:
                val = js[js_key]
            except Exception as e:
                self.fail("{}\n{}".format(e, js))
            self.assertEqual(js[js_key], val, res.__dict__)
        else:
            self.assertIn(str(val), js, res.__dict__)

    @property
    def func(self):
        func = (lambda s: s[5:])(inspect.stack()[1][3])
        return getattr(self.kardia.disb, func)

    def test_getLineItem(self):
        val = f"{self.ledger}|{self.batch}|{self.check}|{self.line_item}"
        self.doTest(
            self.func(
                self.ledger,
                self.year,
                self.month,
                self.batch,
                self.check,
                self.line_item,
            ),
            val=val,
            js_key="name",
        )

    def test_getLineItems(self):
        val = f"{self.ledger}|{self.batch}|{self.check}|{self.line_item}"
        self.doTest(self.func(self.ledger, self.year, self.month, self.batch), val=val)

    def test_createLineItem(self):
        attrs = {
            "ledger": self.ledger,
            "period": self.month,
            "batch": self.batch,
            "disb_id": 5,
            "line_item_no": 1,
            "cash_account": "10152",
            "amount": "250",
            "fund": "RenningerT",
            "account": "60128",
            "payee": "100104",
            "check_no": "EFT",
        }
        line_item = LineItem(**attrs)
        res = self.func(self.year, line_item)
        print(res)
        self.fail("TODO")

    def test_createBatchWithLineItems(self):
        attrs = {
            "cash_account": "1010",
            "amount": Decimal("250.84"),
            "fund": "RenningerT",
            "account": "8000",
            "payee": "100104",
            "check_no": "EFT",
            "comment": "John Doe HRA medical reimbursement",
        }
        line_item_1 = LineItem(**attrs)
        attrs["fund"] = "RBankFund"
        line_item_2 = LineItem(**attrs)
        lis = [line_item_1, line_item_2]
        desc = "HRA Medical reimbursement"
        date = datetime.today()
        res = self.kardia.disb.createBatchWithLineItems(
            self.ledger, self.year, self.month, lis, desc, date, True, True
        )
        print(res)
