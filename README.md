# Kardia Python API

Python libary for the Kardia API

```python
>>> from kardia_api import Kardia
>>> kardia = Kardia(kardia_url, username, password)
>>> res = kardia.gl.getYear(ledger_id, year_id)
>>> res.status_code
200
```

## Table of Contents
- [Kardia Python API](#markdown-header-kardia-python-api)
  - [Table of Contents](#markdown-header-table-of-contents)
  - [Installation](#markdown-header-installation)
  - [Kardia Objects](#markdown-header-kardia-objects)
    - [API](#markdown-header-api)
    - [CRM Config](#markdown-header-crm-config)
    - [Designation](#markdown-header-designation)
    - [Disbursement](#markdown-header-disbursement)
    - [Donor](#markdown-header-donor)
    - [Fund Manager](#markdown-header-fund-manager)
    - [GL](#markdown-header-gl)
    - [Partner](#markdown-header-partner)
    - Report
  - [Modules](#markdown-header-modules)
    - [API Module](#markdown-header-api-module)
    - [App Info](#markdown-header-app-info)
    - [CRM](#markdown-header-crm)
    - [CRM Config](#markdown-header-crm-config-1)
    - [Designation](#markdown-header-designation-1)
    - [Disbursments](#markdown-header-disbursments)
    - [Donor](#markdown-header-donor-1)
    - [Files](#markdown-header-files)
    - [Fund Manager](#markdown-header-fund-manager-1)
    - [GL](#markdown-header-gl-1)
    - [Missionary](#markdown-header-missionary)
    - [Partner](#markdown-header-partner-1)
    - [Partner Search](#markdown-header-partner-search)
    - [Supporter](#markdown-header-supporter)
    - [System Config](#markdown-header-system-config)
  - [URI Params](#markdown-header-uri-params)
    - [Auto](#markdown-header-auto)
    - [Type Properties](#markdown-header-type-properties)
    - [Params Method](#markdown-header-params-method)

## Installation
---
Available on PyPI:

```console
$ python -m pip install -i https://test.pypi.org/simple/ kardia-api
```

## Kardia Objects
---
### APIObject
Parent class for all Kardia objects
### CRM Config
```python
from kardia_api.objects import crm_config_objects as cco
```
**Track**
```python
cco.Track(name: str, description: str, status: Statuses = Statuses.ACTIVE, t_id: int = None, color: str = None)
```
**Step**
```python
cco.Step(track_id: int, name: str, description: str, sequence: int = 1, s_id: int = None)
```
TODO
### Designation
```python
from kardia_api.objects import designation_objects as do
```
**Funding Target**
```python
do.FundingTarget(ledger_id: str, fund_id: str, target_id: int, target_desc: str, review: str = None, amount: str = "0.00", interval: int = Intervals.MONTHLY, start_date: datetime = datetime.today(), end_date: datetime = None)
```
**Admin Fee**
```python
do.AdminFee(fund_id: str, ledger_id: str, fee_type: str, subtype: str = None, percentage: float = None)
```
**Receipting**
```python
do.Receipting(fund_id: str, ledger_id: str, receiptable: bool = True, disposition: str = None)
```
**Receipting Account**
```python
do.ReceiptingAccount(fund_id: str, ledger_id: str, account: str, non_tax_deductible: bool = False, default: bool = False, receipt_comment: str = None)
```

### Disbursement
```python
from kardia_api.objects import disb_objects as do
```
**Line Item**
```python
do.LineItem(ledger: str = None, period: str = None, batch: int = None, disb_id: int = None, line_item_no: int = None, effective_date: datetime = datetime.today(), cash_account: str = None, amount: str = "0.00", fund: str = None, account: str = None, payee: str = None, check_no: str = None, posted: bool = False, gl_posted: bool = False, voided: bool = False, approved_by: str = None, approved_date: datetime = None, paid_by: str = None, paid_date: datetime = None, reconciled: bool = False, comment: str = "")
```

### Donor
```python
from kardia_api.objects import donor_objects as do
```
**Settings**
```python
do.Settings(key: str, ledger: str, account_code: str = None, account_with_donor: str = None, allow_contributions: bool = True, location_id: int = None, contact_id: int = None, org_name_first: bool = True, receipts: str = None, is_daf: bool = False)
```

### Fund Manager
```python
from kardia_api.objects import fundmanager_objects as fo
```
**Settings**
```python
fo.Settings(ledger_id: str, fund: str, partner_key: str, start_date: datetime = None, end_date: datetime = None)
```

### GL
```python
from kardia_api.objects import gl_objects as glo
```
**Account**
```python
glo.Account(code: str, ledger_id: str, desc: str, parent_code: str = None, acc_type: AccountTypes = AccountTypes.ASSET, acc_class: str = "GEN", reporting_level: int = 1, banking_key: str = None, contra: bool = False, posting: bool = True, inverted: bool = False, intrafund_xfer: bool = False, interfund_xfer: bool = False, comment: str = None, legacy_code: str = None, category: str = None)
```
**Fund**
```python
glo.Fund(fund_id: str, ledger_id: str, parent_id: str = None, bal_fund_id: str = None, fund_class: str = None, reporting_level: int = 1, posting: bool = True, external: bool = False, balancing=True, restricted_type: RestrictedTypes = RestrictedTypes.NOT_RESTRICTED, desc: str = None, comments: str = None, legacy_code: str = None)
```
**Period**
```python
glo.Period(period_id: str, ledger_id: str, start_date: datetime, end_date: datetime, parent_id: str = None, status: PeriodStatuses = PeriodStatuses.NEVER_OPENED, summary: bool = False, first_opened: datetime = None, last_closed: datetime = None, archived: datetime = None, desc: str = "", comment: str = "")
```
**Year**
```python
glo.Year(period_id: str, ledger_id: str, start_date: datetime, end_date: datetime, desc: str = "", comment: str = "")
```
**Batch**
```python
glo.Batch(ledger_id: str, period_id: str, batch_id: int = None, desc: str = "", origin: str = "GL", date: datetime = None)
```
**Transaction**
```python
glo.Transaction(ledger_id: str = None, period_id: str = None, batch_id: int = None, journal_id: int = None, date: datetime = datetime.today(), fund: str = None, account_category: AccountCategories = AccountCategories.EXPENSES,account_code: str = None, amount: str = "0.00", isPosted: bool = True, comment: str = None)
```
**Analysis Attribute**
```python
glo.AnalysisAttribute(attr_code: str, ledger_id: str, desc: str, fund_enable: bool, account_enable: bool)
```
**Analysis Attribute Value**
```python
glo.AnalysisAttributeValue(attr_code: str, ledger_id: str, value: str, desc: str = None)
```
**Account Analysis Attribute**
```python
glo.AccountAnalysisAttribute(attr_code: str, ledger_id: str, account_code: str, value: str = None)
```
**Fund Analysis Attribute**
```python
glo.FundAnalysisAttribute(attr_code: str, ledger_id: str, fund: str, value: str = None)
```

### Partner
```python
from kardia_api.objects import partner_objects as po
```
**Partner**
```python
po.Partner(key: str, office: str, parent_key: str = None, p_class: str = "IND", status_code: StatusCodes = StatusCodes.ACTIVE, status_date: datetime = None, p_title: str = None, first_name: str = None, preferred_name: str = None, last_name: str = None, last_name_first: bool = False, localized_name: str = None, suffix: str = None, org_name: str = None, gender: str = None, language: str = None, acquisition: str = None, comments: str = None, record_status_code: RecordStatusCodes = RecordStatusCodes.ACTIVE, no_mail_reason: str = None, no_solicitations: bool = False, no_mail: bool = False, fund: str = None, best_contact: str = None, merged_with: str = None, legacy_key_1: str = None, legacy_key_2: str = None, legacy_key_3: str = None, staff_object = None, address_objects = None, contact_info_objects = None)
```
**Address**
```python
po.Address(key: str, location_id: int, revision_id: int, location_type: str = None, date_effective: datetime = None, date_good_until: datetime = None, purge_date: datetime = None, in_care_of: str = None, addr1: str = None, addr2: str = None, addr3: str = None, city: str = None, state_province: str = None, country: str = None, postal_code: str = None, postal_mode: str = None, bulk_postal_code: str = None, certified_date: datetime = None, postal_status: str = None, postal_barcode: str = None, record_status_code: str = RecordStatusCodes.ACTIVE, comments: str = None):
```
**Contact Info**
```python
po.ContactInfo(key: str, contact_id: int, contact_type: str, location_id: str = None, phone_country: str = None, phone_area_city: str = None, contact_data: str = None, record_status_code: str = RecordStatusCodes.ACTIVE, comments: str = None)
```
**Staff**
```python
po.Staff(key: str, is_staff: bool = None, kardia_login: str = None, kardiaweb_login: str = None, preferred_email: int = None, preferred_location: int = None)
```

## Modules
---
### API Module
Parent class for all Kardia API modules

### App Info
```python
>>> TODO
```
#### Methods
- 

### CRM
```python
>>> kardia.crm
```
#### Methods
- getTracks()
- getTrack(track: str)
- getTrackSteps(track: str)
- getTrackStep(track: str, step: str)
- getStepCollaborators(track: str, step: str)
- getStepCollaborator(track: str, step: str, track_id: int, step_id: int, collaborator_id: str)
- getTrackCollaborators(track: str)
- TODO

### CRM Config
```python
>>> TODO
```
#### Methods
- 

### Designation
```python
>>> kardia.designation
```
#### Methods
- getFundFundingTargets(ledger_id: str, fund_id: str)
- getFundingTargets()
- getFundingTarget(ledger_id: str, fund_id: str, target_id: str)
- getNextFundingTargetID(ledger_id: str, fund_id: str)
- createFundingTarget(funding_target: FundingTarget)
- getFundAdminFees(ledger_id: str, fund_id: str)
- getAdminFees()
- getAdminFee(ledger_id: str, fund_id: str)
- createAdminFee(admin_fee: AdminFee)
- getFundReceiptings(ledger_id: str, fund_id: str)
- getReceiptings()
- getReceipting(ledger_id: str, fund_id: str)
- createReceipting(receipting: Receipting)
- getFundReceiptingAccounts(ledger_id: str, fund_id: str)
- getReceiptingAccounts()
- getReceiptingAccount(ledger_id: str, fund_id: str, account_id: str)
- createReceiptingAccount(receipting_account: ReceiptingAccount)

### Disbursments
```python
>>> kardia.disb
```
#### Methods
- getLineItem(ledger: str, year: str, month: str, batch: int, check: int, line_item: int)
- getLineItems(ledger: str, year: str, month: str, batch: int)
- createLineItem(year: str, line_item: LineItem)
- createLineItems(year: str, line_items: List[LineItem])
- createBatchWithLineItems(ledger: str, year: str, month: str, line_items: List[LineItem], desc: str = "", date: datetime = None, post: bool = False, gl_post: bool = False)

### Donor
```python
>>> kardia.donor
```
#### Methods
- getDonor(partner_id: str)
- getDonors()
- getDonorGift(partner_id: str, gift_number: int):
- getDonorGifts(partner_id: str)
- getGiftReceipt(partner_id: str, gift_number: int)
- getYear(partner_id: str, year_id: str)
- getYears(partner_id: str)
- getYearGift(partner_id: str, year_id: str, gift_number: int)
- getYearGifts(partner_id: str, year_id: str)
- getYearGiftReceipt(partner_id: str, year_id: str, gift_number: int)
- getYearFund(partner_id: str, year_id: str, fund_id: str)
- getYearFunds(partner_id: str, year_id: str)
- getYearFundGift(partner_id: str, year_id: str, fund_id: str, gift_number: int)
- getYearFundGifts(partner_id: str, year_id: str, fund_id: str)
- getYearFundGiftReceipt(partner_id: str, year_id: str, fund_id: str, gift_number: int)
- getFund(partner_id: str, fund_id: str)
- getFunds(partner_id: str)
- getFundGift(partner_id: str, fund_id: str, gift_number: int)
- getFundGifts(partner_id: str, fund_id: str)
- getFundGiftReceipt(partner_id: str, fund_id: str, gift_number: int)
- getFundYear(partner_id: str, fund_id: str, year_id: str)
- getFundYears(partner_id: str, fund_id: str)
- getFundYearGift(partner_id: str, fund_id: str, year_id: str, gift_number: int)
- getFundYearGifts(partner_id: str, fund_id: str, year_id: str)
- getFundYearGiftReceipt(partner_id: str, fund_id: str, year_id: str, gift_number: int)
- getGivingInfo(partner_id: str, year_id: str)
- getDonorInfo(partner_id: str, year_id: str)
- getSettings()
- getSetting(donor_id: str, ledger_id: str)
- createSettings(settings: Settings)

### Files
```python
>>> TODO
```

### Fund Manager
```python
>>> kardia.fundmanager
```
#### Methods
- getSettings()
- getSetting(ledger_id: str, fund_id: str, manager_id: str)
- createSettings(settings: Settings)

### GL
```python
>>> kardia.gl
```
#### Methods
- getAccounts(ledger_id: str)
- getAccount(ledger_id: str, account_id: str)
- createAccount(account: Account)
- getAllFunds(ledger_id)
- getFunds(ledger_id)
- getFund(ledger_id, fund_id)
- createFund(fund: Fund)
- getSubfunds(ledger_id, fund_id)
- getSubfund(ledger_id, fund_id, subfund_id)
- getBatches(ledger_id, year_id, period_id)
- getBatch(ledger_id, year_id, period_id, batch_id)
- getNextBatchId(ledger_id)
- createBatch(year_id, batch: Batch)
- getPeriods(ledger_id, year_id)
- getPeriod(ledger_id, year_id, period_id)
- createPeriod(period: Period)
- getYears(ledger_id: str)
- getYear(ledger_id: str, year_id: str)
- createYear(year: Year)
- createMonth(ledger_id: str, year_id: str, month: int, month_id: str = None)
- getTransactions(ledger_id, year_id, period_id, batch_id)
- createJEWithTrans(ledger_id, year_id, period_id, transactions: List[Transaction], journal_id=1, batch_id=None, desc="", post=True)
- createTransaction(year_id: str, trans: Transaction)
- createTransactions(year_id: str, transactions: List[Transaction])
- getTransaction(ledger_id: str, year_id: str, period_id: str, batch_id: int, trans_id: int, journal_id: int = 1)
- getAnalysisAttributes(ledger_id: str)
- getAnalysisAttribute(ledger_id: str, attr_code: str)
- createAnalysisAttribute(analysis_attribute: AnalysisAttribute):
- getAnalysisAttributeValues(ledger_id: str)
- getAnalysisAttributeValue(ledger_id: str, attr_code: str, value: str)
- createAnalysisAttributeValue(analysis_attribute_value: AnalysisAttributeValue):
- getAccountAnalysisAttributes(ledger_id: str)
- getAccountAnalysisAttribute(ledger_id: str, account_code: str, attr_code: str)
- createAccountAnalysisAttribute(account_analysis_attribute: AccountAnalysisAttribute):
- getAccountAccountAnalysisAttributes(ledger_id: str, account_code: str)
- getFundAnalysisAttributes(ledger_id: str)
- getFundAnalysisAttribute(ledger_id: str, fund_id: str, attr_code: str)
- createFundAnalysisAttribute(fund_analysis_attribute: FundAnalysisAttribute):
- getFundFundAnalysisAttributes(ledger_id: str, fund_id: str)

### Missionary
```python
>>> TODO
```
#### Methods
- 

### Partner
```python
>>> kardia.partner
```
#### Methods
- getPartners()
- getPartner(partner_id: str)
- createPartner(partner: Partner)
- getPartnerAddresses(partner_id: str)
- getPartnerAddress(partner_id, location_id: str)
- getNextPartnerAddressID(partner_id: str)
- createPartnerAddress(address: Address):
- getPartnerContactInfos(partner_id: str)
- getPartnerContactInfo(partner_id: str, contact_id: str)
- getNextPartnerContactInfoID(partner_id: str)
- createPartnerContactInfo(contact_info: ContactInfo):
- getPartnerSubscriptions(partner_id: str)
- getPartnerSubscription(partner_id: str, list_id: str)
- getStaff()
- createStaff(staff: Staff):
- getStaffMember(staff_id: str)
- getStaffLogins()
- getStaffLogin(username: str)
- getContactTypes()
- getContactType(contact_type)
- getTests()
- getTest(test)
- getNextPartnerKey()

### Partner Search
```python
>>> TODO
```
#### Methods
- 

### Supporter
```python
>>> TODO
```
#### Methods
- 

### System Config
```python
>>> TODO
```
#### Methods


## URI Params
---
Kardia uses vairous URI parameters to determine what type should be returned and how it is formatted. See [Centrallix REST Interface URI Parameters](https://github.com/LightSys/centrallix/blob/b9d04ed04bdd8b194790e74f2ee67f4c7bc3c500/centrallix-sysdoc/JSON-REST.md#rest-interface-uri-parameters). There are three ways that these paramaters can be set.

### Auto
API modules will by default try to determine the correct URI params.

```python
>>> res = kardia.gl.getFunds("LEDGER")
>>> res.url
.../api/gl/LEDGER/Funds/?cx__mode=rest&cx__res_type=collection

>>> res = kardia.gl.getFund("LEDGER", "10001")
>>> res.url
.../api/gl/LEDGER/Funds/10001|LEDGER?cx__mode=rest&cx__res_format=attrs
```

### Type Properties
API modules can set the *res_type* and that type's *res_format* with the two properties: **collection** and **element**. This allows for chaining methods.

```python
>>> res = kardia.gl.element.getFunds("LEDGER")
>>> res.url
.../api/gl/LEDGER/Funds/?cx__mode=rest&cx__res_type=element&cx__res_format=attrs

>>> res = kardia.gl.collection.getFund("LEDGER", "10001")
>>> res.url
.../api/gl/LEDGER/Funds/10001|LEDGER?cx__mode=rest&cx__res_type=collection
```

### Params Method
API modules have a *setParams()* method which allows for setting any and all of the URI params. This also allows for chaining methods.

```python
>>> res = kardia.gl.setParams(res_type="collection", res_format="attrs", res_attrs="full", res_levels=1).getFunds("LEDGER")
>>> res.url
.../api/gl/LEDGER/Funds/?cx__mode=rest&cx__res_type=collection&cx__res_format=attrs&cx__res_attrs=full
```