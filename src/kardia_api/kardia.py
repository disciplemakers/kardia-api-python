from .modules import (
    app_info,
    config,
    crm,
    crm_config,
    designation,
    disbursements,
    donor,
    files,
    fundmanager,
    gift,
    gl,
    partner,
    report,
    acctconfig,
)


class ResTypes:
    COLLECTION = "collection"
    ELEMENT = "element"
    BOTH = "both"


class ResFormats:
    ATTRS = "attrs"
    AUTO = "auto"
    CONTENT = "content"
    BOTH = "both"


class ResAttrs:
    BASIC = "basic"
    FULL = "full"
    NONE = "none"


class ResLevels:
    ONE = "1"
    TWO = "2"
    THREE = "3"
    FOUR = "4"
    FIVE = "5"


class Kardia:
    def __init__(self, kardia_url, user, pw):
        self.kardia_config = {"kardia_url": kardia_url, "user": user, "pw": pw}
        self._crm = None
        self._crm_config = None
        self._designation = None
        self._disb = None
        self._donor = None
        self._fundmanager = None
        self._gift = None
        self._gl = None
        self._partner = None
        self._report = None
        self._app_info = None
        self._files = None
        self._config = None
        self._sys_config = None
        self._acctconfig = None

    @property
    def crm(self):
        if not self._crm:
            self._crm = crm.CRM(**self.kardia_config)
        return self._crm

    @property
    def crm_config(self):
        if not self._crm_config:
            self._crm_config = crm_config.CRM_Config(**self.kardia_config)
        return self._crm_config

    @property
    def designation(self) -> designation.Designation:
        if not self._designation:
            self._designation = designation.Designation(**self.kardia_config)
        return self._designation

    @property
    def disb(self) -> disbursements.Disbursements:
        if not self._disb:
            self._disb = disbursements.Disbursements(**self.kardia_config)
        return self._disb

    @property
    def donor(self) -> donor.Donor:
        if not self._donor:
            self._donor = donor.Donor(**self.kardia_config)
        return self._donor

    @property
    def fundmanager(self) -> fundmanager.FundManager:
        if not self._fundmanager:
            self._fundmanager = fundmanager.FundManager(**self.kardia_config)
        return self._fundmanager

    @property
    def gift(self) -> gift.Gift:
        if not self._gift:
            self._gift = gift.Gift(**self.kardia_config)
        return self._gift

    @property
    def gl(self):
        if not self._gl:
            self._gl = gl.GL(**self.kardia_config)
        return self._gl

    @property
    def partner(self) -> partner.Partner:
        if not self._partner:
            self._partner = partner.Partner(**self.kardia_config)
        return self._partner

    @property
    def report(self) -> report.Report:
        if not self._report:
            self._report = report.Report(**self.kardia_config)
        return self._report

    @property
    def app_info(self) -> app_info.AppInfo:
        if not self._app_info:
            self._app_info = app_info.AppInfo(**self.kardia_config)
        return self._app_info

    @property
    def files(self) -> files.Files:
        if not self._files:
            self._files = files.Files(**self.kardia_config)
        return self._files

    @property
    def config(self) -> config.Config:
        if not self._config:
            self._config = config.Config(**self.kardia_config)
        return self._config

    @property
    def acctconfig(self) -> acctconfig.AccountConfig:
        if not self._acctconfig:
            self._acctconfig = acctconfig.AccountConfig(**self.kardia_config)
        return self._acctconfig

    # @property
    # def sys_config(self) -> sys_config.SysConfig:
    #     if not self._sys_config:
    #         self._sys_config = sys_config.SysConfig(**self.kardia_config)
    #     return self._sys_config
