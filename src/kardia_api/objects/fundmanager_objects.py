from datetime import datetime
from enum import Enum
from .api_object import APIObject


class FundManagerObject(APIObject):
    pass


class Settings(FundManagerObject):

    def __init__(self, ledger_id: str, fund: str, partner_key: str, start_date: datetime = None, end_date: datetime = None):

        super().__init__()
        self.a_ledger_number = ledger_id
        self.a_fund = fund
        self.p_staff_partner_key = partner_key
        self.p_start_date = start_date
        self.p_end_date = end_date
