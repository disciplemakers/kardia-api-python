from datetime import datetime
from decimal import Decimal
from .api_object import APIObject


class DisbObjects(APIObject):
    pass


class LineItem(DisbObjects):

    def __init__(self, ledger: str = None, period: str = None, batch: int = None, disb_id: int = None, line_item_no: int = None,
                 effective_date: datetime = datetime.today(), cash_account: str = None, amount: Decimal = Decimal("0.00"), fund: str = None,
                 account: str = None, payee: str = None, check_no: str = None, posted: bool = False, gl_posted: bool = False,
                 voided: bool = False, approved_by: str = None, approved_date: datetime = None, paid_by: str = None,
                 paid_date: datetime = None, reconciled: bool = False, comment: str = ""
                 ) -> None:
        super().__init__()
        self.a_ledger_number = ledger
        self.a_batch_number = batch
        self.a_disbursement_id = disb_id
        self.a_line_item = line_item_no
        self.a_period = period
        self.a_effective_date = effective_date if effective_date else datetime.now()
        self.a_cash_account_code = cash_account
        self.a_amount = amount
        self.a_fund = fund
        self.a_account_code = account
        self.a_payee_partner_key = payee
        self.a_check_number = check_no
        self.a_posted = posted.real
        self.a_posted_to_gl = gl_posted.real
        self.a_voided = voided.real
        self.a_approved_by = approved_by
        self.a_approved_date = approved_date
        self.a_paid_by = paid_by
        self.a_paid_date = paid_date
        self.a_reconciled = reconciled.real
        self.a_comment = comment
        self.required_attrs = [
            "a_ledger_number",
            "a_batch_number",
            "a_disbursement_id",
            "a_line_item",
            "a_period",
            "a_effective_date",
            "a_cash_account_code",
            "a_amount",
            "a_fund",
            "a_account_code",
            "a_payee_partner_key",
        ]
