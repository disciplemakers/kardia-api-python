from datetime import datetime
from decimal import Decimal
from enum import Enum
from .api_object import APIObject


class DesignationObject(APIObject):
    pass


class FundingTarget(DesignationObject):

    class Intervals(Enum):
        """List of Kardia funding target intervals
        """
        ABSOLUTE = 0
        MONTHLY = 1
        QUARTERLY = 3
        ANNUALLY = 12

    def __init__(self, ledger_id: str, fund_id: str, target_id: int, target_desc: str, review: str = None,
                 amount: Decimal = Decimal("0.00"), interval: int = Intervals.MONTHLY,
                 start_date: datetime = datetime.today(), end_date: datetime = None):

        super().__init__()
        self.a_ledger_number = ledger_id
        self.a_fund = fund_id
        self.a_target_id = target_id
        self.a_target_desc = target_desc
        self.a_review = review
        self.a_amount = amount
        self.a_interval = interval
        self.a_start_date = start_date
        self.a_end_date = end_date


class AdminFee(DesignationObject):

    def __init__(self, fund_id: str, ledger_id: str, fee_type: str, subtype: str = None, percentage: float = None):

        super().__init__()
        self.a_fund = fund_id
        self.a_ledger_number = ledger_id
        self.a_admin_fee_type = fee_type
        self.a_default_subtype = subtype
        self.a_percentage = percentage


class Receipting(DesignationObject):

    class Dispositions(Enum):
        """List of Kardia donor management fund_id dispositions
        """
        NOT_INTERESTING = "N"
        ONE_TIME_GIFTS_TYPICAL = "O"
        RECURRING_GIFTS_TYPICAL = "R"

    def __init__(self, fund_id: str, ledger_id: str, receiptable: bool = True, disposition: str = None):

        super().__init__()
        self.a_fund = fund_id
        self.a_ledger_number = ledger_id
        self.a_receiptable = receiptable
        self.a_disposition = disposition


class ReceiptingAccount(DesignationObject):

    def __init__(self, fund_id: str, ledger_id: str, account: str, non_tax_deductible: bool = False,
                 default: bool = False, receipt_comment: str = None):

        super().__init__()
        self.a_fund = fund_id
        self.a_ledger_number = ledger_id
        self.a_account_code = account
        self.a_non_tax_deductible = non_tax_deductible
        self.a_is_default = default
        self.a_receipt_comment = receipt_comment
