from datetime import datetime
from enum import Enum
from .api_object import APIObject


class DonorObject(APIObject):
    pass


class Settings(DonorObject):

    class ReceiptPreferences(Enum):
        """List of Kardia donor receipt preferences
        """
        ANNUAL = "A"
        IMMEDIATE = "I"
        NONE = "N"

    def __init__(self, key: str, ledger: str, account_code: str = None, account_with_donor: str = None, allow_contributions: bool = True,
                 location_id: int = None, contact_id: int = None, org_name_first: bool = True, receipts: str = None, is_daf: bool = False):

        super().__init__()
        self.p_partner_key = key
        self.a_gl_ledger_number = ledger
        self.a_gl_account_code = account_code
        self.p_account_with_donor = account_with_donor
        self.p_allow_contributions = allow_contributions
        self.p_location_id = location_id
        self.p_contact_id = contact_id
        self.p_org_name_first = org_name_first
        self.p_receipt_desired = receipts
        self.p_is_daf = is_daf