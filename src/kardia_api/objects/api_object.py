import re
from datetime import datetime
from decimal import Decimal
from enum import Enum


class APIObject():

    def __init__(self) -> None:
        self.s_created_by = "api"
        self.s_modified_by = self.s_created_by
        self.s_date_created = self.getKardiaDatetime()
        self.s_date_modified = self.s_date_created
        setattr(self, "__cx_osml_control", None)
        self.required_attrs = []

    def checkValid(self) -> None:
        msg = "Invalid Transaction: {} is required."
        for attr in self.required_attrs:
            if not getattr(self, attr):
                raise Exception(msg.format(attr))

    def serialize(self) -> dict:
        self.checkValid()
        d = self.__dict__
        serialized = {}

        for key, val in d.items():
            if re.search(r"^\w{1}_.*", key):
                if isinstance(val, bool):
                    val = val.real
                elif isinstance(val, Enum):
                    val = val.value
                elif isinstance(val, datetime):
                    val = self.getKardiaDatetime(val)
                elif isinstance(val, Decimal):
                    val = self.getKardiaMoney(val)
                serialized[key] = val
        return serialized
        # return {key: d[key] for key in d if re.search(r"^\w{1}_.*", key)}

    def getKardiaDatetime(self, date: datetime = datetime.today()) -> dict:
        return {
            "year": date.year,
            "month": date.month,
            "day": date.day,
            "hour": date.hour,
            "minute": date.minute,
            "second": date.second
        }

    def getKardiaMoney(self, amount: Decimal = Decimal("0.00")):
        whole = int(amount - amount%1)
        frac = int(amount%1 * 10000)
        return {
            "wholepart": whole if amount >= 0 else whole -1,
            "fractionpart": frac if amount >= 0 else frac + 10000
        }
