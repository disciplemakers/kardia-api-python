from enum import Enum
from .api_object import APIObject


class CRM_ConfigObject(APIObject):
    pass


class Track(CRM_ConfigObject):

    class Statuses(Enum):
        ACTIVE = "A"
        OBSOLETE = "O"

    def __init__(self, name: str, description: str, status: Statuses = Statuses.ACTIVE, t_id: int = None, color: str = None) -> None:
        super().__init__()
        self.e_track_id = t_id
        self.e_track_name = name
        self.e_track_description = description
        self.e_track_color = color
        self.e_track_status = status.value


class Step(CRM_ConfigObject):

    def __init__(self, track_id: int, name: str, description: str, sequence: int = 1, s_id: int = None, ) -> None:
        super().__init__()
        self.e_track_id = track_id
        self.e_step_id = s_id
        self.e_step_name = name
        self.e_step_description = description
        self.e_step_sequence = sequence


class TrackCollaborator(CRM_ConfigObject):

    def __init__(self, track_id: int, partner_id: str, collab_type_id: int, comments=None) -> None:
        super().__init__()
        self.e_track_id = track_id
        self.p_collab_partner_key = partner_id
        self.e_collab_type_id = collab_type_id
        self.e_comments = comments


class StepCollaborator(TrackCollaborator):

    def __init__(self, track_id: int, step_id: int, partner_id: str, collab_type_id: int, comments) -> None:
        super().__init__(track_id, partner_id, collab_type_id, comments=comments)
        self.e_step_id = step_id


class StepRequirement(CRM_ConfigObject):

    class Whom(Enum):
        PARTNER = "P"
        ORGANIZATIONS = "O"
        EITHER = "E"

    def __init__(self, track_id: int, step_id: int, req_id: int, name: str, whom: Whom, waivable: bool,
                 sequence: int = 1, active: bool = True, due_days_from_step: int = None,
                 due_days_from_req: int = None, due_days_from_req_id: int = None, req_doc_type_id: int = None) -> None:
        super().__init__()
        self.e_track_id = track_id
        self.e_step_id = step_id
        self.e_req_id = req_id
        self.e_req_name = name
        self.e_due_days_from_step = due_days_from_step
        self.e_due_days_from_req = due_days_from_req
        self.e_due_days_from_req_id = due_days_from_req_id
        self.e_req_whom = whom.value
        self.e_req_doc_type_id = req_doc_type_id
        self.e_req_waivable = waivable.real
        self.e_req_sequence = sequence
        self.e_is_active = active.real
