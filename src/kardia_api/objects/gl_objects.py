from datetime import datetime
from decimal import Decimal
from enum import Enum
from .api_object import APIObject


class GLObject(APIObject):
    pass


class Account(GLObject):

    class AccountTypes(Enum):
        """List of Kardia account types
        """
        ASSET = "A"
        LIABILITY = "L"
        EQUITY = "Q"
        REVENUE = "R"
        EXPENSE = "E"

    def __init__(self, code: str, ledger_id: str, desc: str, parent_code: str = None, acc_type: AccountTypes = AccountTypes.ASSET,
                 acc_class: str = "GEN", reporting_level: int = 1, banking_key: str = None, contra: bool = False,
                 posting: bool = True, inverted: bool = False, intrafund_xfer: bool = False, interfund_xfer: bool = False,
                 comment: str = None, legacy_code: str = None, category: str = None):
        """init

        Args:
            code (str): Account code
            ledger_id (str): Ledger containing the account
            desc (str): Description
            parent_code (str, optional): Accounts parent. Defaults to None.
            acc_type (str, optional): Account type. Defaults to AccountTypes.ASSET.
            acc_class (str, optional): Account class. Defaults to "GEN".
            reporting_level (int, optional): Reporting level. Defaults to 1.
            banking_key (str, optional): Banking key. Defaults to None.
            contra (bool, optional): ???. Defaults to False.
            posting (bool, optional): Whether or not the account is posting. Defaults to True.
            inverted (bool, optional): Whether or not the account is inverted. Defaults to False.
            intrafund_xfer (bool, optional): Whether or not the account is intrafund transfer. Defaults to False.
            interfund_xfer (bool, optional): Whether or not the account is interfund transfer. Defaults to False.
            comment (str, optional): Comment. Defaults to None.
            legacy_code (str, optional): Account legacy code. Defaults to None.
            category (str, optional): Account category. Defaults to None.
        """
        super().__init__()
        self.a_account_code = code
        self.a_ledger_number = ledger_id
        self.a_parent_account_code = parent_code
        self.a_acct_type = acc_type.value
        self.a_account_class = acc_class
        self.a_reporting_level = reporting_level
        self.p_banking_details_key = banking_key
        self.a_is_contra = contra.real
        self.a_is_posting = posting.real
        self.a_is_inverted = inverted.real
        self.a_is_intrafund_xfer = intrafund_xfer.real
        self.a_is_interfund_xfer = interfund_xfer.real
        self.a_acct_desc = desc
        self.a_acct_comment = comment
        self.a_legacy_code = legacy_code
        self.a_default_category = category


class Fund(GLObject):

    class RestrictedTypes(Enum):
        """List of Kardia restricted types
        """
        NOT_RESTRICTED = "N"
        TEMP_RESTRICTED = "T"
        PERM_RESTRICTED = "P"

    def __init__(self, fund_id: str, ledger_id: str, parent_id: str = None, bal_fund_id: str = None, fund_class: str = None,
                 reporting_level: int = 1, posting: bool = True, external: bool = False, balancing=True,
                 restricted_type: RestrictedTypes = RestrictedTypes.NOT_RESTRICTED, desc: str = None, comments: str = None,
                 legacy_code: str = None):
        super().__init__()
        self.a_fund = fund_id
        self.a_ledger_number = ledger_id
        self.a_parent_fund = parent_id
        self.a_bal_fund = bal_fund_id
        self.a_fund_class = fund_class
        self.a_reporting_level = reporting_level
        self.a_is_posting = posting.real
        self.a_is_external = external.real
        self.a_is_balancing = balancing.real
        self.a_restricted_type = restricted_type.value
        self.a_fund_desc = desc
        self.a_fund_comments = comments
        self.a_legacy_code = legacy_code


class Period(GLObject):

    class PeriodStatuses(Enum):
        NEVER_OPENED = "N"
        OPENED = "O"
        CLOSED = "C"
        ARCHIVED = "A"

    def __init__(self, period_id: str, ledger_id: str, start_date: datetime, end_date: datetime, parent_id: str = None,
                 status: PeriodStatuses = PeriodStatuses.NEVER_OPENED, summary: bool = False, first_opened: datetime = None,
                 last_closed: datetime = None, archived: datetime = None, desc: str = "", comment: str = ""):
        super().__init__()
        self.a_period = period_id
        self.a_ledger_number = ledger_id
        self.a_parent_period = parent_id
        self.a_status = status.value
        self.a_summary_only = summary.real
        self.a_start_date = start_date if start_date else datetime.now()
        self.a_end_date = end_date if end_date else datetime.now()
        self.a_first_opened = first_opened if first_opened else None
        self.a_last_closed = last_closed if last_closed else None
        self.a_archived = archived if archived else None
        self.a_period_desc = desc
        self.a_period_comment = comment

    def checkValid(self) -> None:
        """Checks to see if the transaction is valid for entry.

        Raises:
            Exception: If raiseError is True and the transaction is invalid.

        Returns:
            bool: Whether or not the transaction is valid.
        """
        super().checkValid()
        if (not self.a_parent_period and not self.a_summary_only):
            raise Exception("Top level periods (years) must be summary only.")


class Year(Period):

    def __init__(self, period_id: str, ledger_id: str, status: Period.PeriodStatuses = Period.PeriodStatuses.NEVER_OPENED,
                 start_date: datetime = datetime.now(), end_date: datetime = datetime.now(), first_opened: datetime = None, last_closed: datetime = None,
                 archived: datetime = None, desc: str = "", comment: str = ""):
        super().__init__(period_id, ledger_id, start_date, end_date,
                         None, status, True, first_opened, last_closed, archived, desc, comment)


class Batch(GLObject):

    def __init__(self, ledger_id: str, period_id: str, batch_id: int = None, desc: str = "", origin: str = "GL", date: datetime = None):
        super().__init__()
        self.a_batch_number = batch_id
        self.a_ledger_number = ledger_id
        self.a_period = period_id
        self.a_batch_desc = desc
        self.a_next_journal_number = 1
        self.a_next_transaction_number = 1
        self.a_default_effective_date = date if date else datetime.now()
        self.a_origin = origin
        self.s_process_code = None
        self.s_process_status_code = None
        self.a_corrects_batch_number = None


class Transaction(GLObject):

    class AccountCategories(Enum):
        """List of Kardia account categories
        """
        # TODO These might change or need to be set based on the leger config
        NONCURRENT_ASSET = "10"
        CURRENT_ASSET = "11"
        INTERFUND_ASSET = "19"
        LIABILITY = "20"
        OPENING_EQUITY = "30"
        REVENUE = "40"
        WITHIN_FUND_TRANSFER_REVENUE = "41"
        INTERFUND_TRANSFER_REVENUE = "42"
        EXPENSES = "50"
        WITHIN_FUND_TRANSFER_EXPENSE = "51"
        INTERFUND_TRANSFER_EXPENSE = "52"

    def __init__(self, ledger_id: str = None, period_id: str = None, batch_id: int = None, journal_id: int = None,
                 date: datetime = datetime.today(), transaction_type: str = "T", fund: str = None,
                 account_category: AccountCategories = AccountCategories.EXPENSES, account_code: str = None,
                 amount: Decimal = Decimal("0.00"), isPosted: bool = True, origin: str = "GL", legacy_code: str = None, comment: str = None):
        """init

        Args:
            ledger_id (str, optional): Ledger the transaction belongs to. Defaults to None.
            period_id (str, optional): Period the transaction belongs to. Defaults to None.
            batch_id (int, optional): Batch the transaction belongs to. Defaults to None.
            journal_id (int, optional): Journal entry the transaction belongs to. Defaults to None.
            date (datetime, optional): Effective date for the transaction. Defaults to datetime.today().
            transaction_type (str, optional): B=Beginning balance, E=Ending, T=Transaction (normal). Defaults to "T".
            fund (str, optional): Fund the transaction hits. Defaults to None.
            account_category (str, optional): See gl.AccountCategories. Defaults to AccountCategories.EXPENSES.
            account_code (str, optional): Account code for the transaction. Defaults to None.
            amount (Decimal, optional): Amount. Defaults to Decimal("0.00").
            isPosted (bool, optional): Is the transaction posted? Defaults to True.
            origin (str, optional): Subsystem of origin. Defaults to "GL".
            legacy_code (str, optional): Legacy system ID. Defaults to None.
            comment (str, optional): Comment. Defaults to None.
        """
        super().__init__()
        self.a_ledger_number = ledger_id
        self.a_batch_number = batch_id
        self.a_journal_number = journal_id
        self.a_transaction_number = None
        self.a_period = period_id
        self.a_effective_date = date if date else datetime.now()
        self.a_transaction_type = transaction_type
        self.a_fund = fund
        self.a_account_category = account_category
        self.a_account_code = account_code
        self.a_amount = amount
        self.a_posted = int(isPosted)
        self.a_modified = 0
        self.a_corrected = 0
        self.a_correcting = 0
        self.a_corrected_batch = None
        self.a_corrected_journal = None
        self.a_corrected_transaction = None
        self.a_reconciled = 1
        self.a_postprocessed = 1
        self.a_postprocess_type = "XX"
        self.a_origin = origin
        self.a_recv_document_id = None
        self.a_sent_document_id = None
        self.p_ext_partner_id = None
        self.p_int_partner_id = None
        self.a_legacy_code = legacy_code
        self.a_receipt_sent = 0
        self.a_receipt_desired = 0
        self.a_first_gift = 0
        self.a_gift_type = None
        self.a_goods_provided = Decimal("0.00")
        self.a_gift_received_date = None
        self.a_gift_postmark_date = None
        self.a_comment = comment
        self.required_attrs = [
            "a_ledger_number",
            "a_batch_number",
            "a_period",
            "a_fund",
            "a_account_code"
        ]


class AnalysisAttribute(GLObject):

    def __init__(self, attr_code: str, ledger_id: str, desc: str, fund_enable: bool, account_enable: bool):

        super().__init__()
        self.a_attr_code = attr_code
        self.a_ledger_number = ledger_id
        self.a_desc = desc
        self.a_fund_enab = fund_enable
        self.a_acct_enab = account_enable


class AnalysisAttributeValue(GLObject):

    def __init__(self, attr_code: str, ledger_id: str, value: str, desc: str = None):

        super().__init__()
        self.a_attr_code = attr_code
        self.a_ledger_number = ledger_id
        self.a_value = value
        self.a_desc = desc


class AccountAnalysisAttribute(GLObject):

    def __init__(self, attr_code: str, ledger_id: str, account_code: str, value: str = None):

        super().__init__()
        self.a_attr_code = attr_code
        self.a_ledger_number = ledger_id
        self.a_account_code = account_code
        self.a_value = value


class FundAnalysisAttribute(GLObject):

    def __init__(self, attr_code: str, ledger_id: str, fund: str, value: str = None):

        super().__init__()
        self.a_attr_code = attr_code
        self.a_ledger_number = ledger_id
        self.a_fund = fund
        self.a_value = value
