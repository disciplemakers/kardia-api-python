from datetime import datetime
from decimal import Decimal
from enum import Enum
from .api_object import APIObject


class GiftObject(APIObject):
    pass


class Gift(GiftObject):

    class GiftTypes(Enum):
        """List of Kardia gift types
        """
        CASH = "C"
        CHECK = "K"
        CREDIT_CARD = "D"
        EFT = "E"

    class ReceiptPreferences(Enum):
        """List of Kardia donor receipt preferences
        """
        ANNUAL = "A"
        IMMEDIATE = "I"
        NONE = "N"


    def __init__(self, ledger: str = None, batch: int = None, gift: int = None, period: str = None, amount: Decimal = Decimal("0.00"),
                 gift_type: GiftTypes = GiftTypes.CHECK, foreign_amount: Decimal = None,
                 foreign_currency: str = None, foreign_currency_exchange_rate: float = None, foreign_currency_date: datetime = None,
                 posted: bool = False, posted_to_gl: bool = False, receipt_number: str = None, partner: str = None,
                 ack_partner: str = None, pass_partner: str = None, receipt_sent: bool = False, ack_receipt_sent: bool = False,
                 receipt_desired: ReceiptPreferences = ReceiptPreferences.IMMEDIATE, ack_receipt_desired: ReceiptPreferences = None, first_gift: bool = False,
                 goods_provided: Decimal = Decimal("0.00"), received_date: datetime = None, postmark_date: datetime = None,
                 receipt_sent_date: datetime = None, ack_receipt_sent_date: datetime = None, comment: str = None, item_objects = []):

        super().__init__()
        self.a_ledger_number = ledger
        self.a_batch_number = batch
        self.a_gift_number = gift
        self.a_period = period
        self.a_amount = amount
        self.a_foreign_amount = foreign_amount
        self.a_foreign_currency = foreign_currency
        self.a_foreign_currency_exch_rate = foreign_currency_exchange_rate
        self.a_foreign_currency_date = foreign_currency_date
        self.a_posted = posted
        self.a_posted_to_gl = posted_to_gl
        self.a_gift_type = gift_type
        self.a_receipt_number = receipt_number
        self.p_donor_partner_id = partner
        self.p_ack_partner_id = ack_partner
        self.p_pass_partner_id = pass_partner
        self.a_receipt_sent = receipt_sent
        self.a_ack_receipt_sent = ack_receipt_sent
        self.a_receipt_desired = receipt_desired
        self.a_ack_receipt_desired = ack_receipt_desired
        self.a_first_gift = first_gift
        self.a_goods_provided = goods_provided
        self.a_gift_received_date = received_date
        self.a_gift_postmark_date = postmark_date
        self.a_receipt_sent_date = receipt_sent_date
        self.a_ack_receipt_sent_date = ack_receipt_sent_date
        self.a_comment = comment
        self.item_objects = item_objects
        

class GiftItem(GiftObject):

    class GiftTypes(Enum):
        """List of Kardia gift types
        """
        CASH = "C"
        CHECK = "K"
        CREDIT_CARD = "D"
        EFT = "E"

    def __init__(self, ledger: str = None, batch: int = None, gift: int = None, split: int = None, period: str = None,
                 fund: str = None, account_code: str = None, amount: Decimal = Decimal("0.00"),
                 foreign_amount: Decimal = None, foreign_currency: str = None,
                 foreign_currency_exchange_rate: float = None, foreign_currency_date: datetime = None,
                 document_id: str = None, account_hash: str = None, check_front_image: str = None, check_back_image: str = None,
                 posted: bool = False, posted_to_gl: bool = False, admin_fee: float = None, admin_fee_subtype: str = None,
                 calc_admin_fee: float = None, calc_admin_fee_type: str = None, calc_admin_fee_subtype: str = None,
                 recip_partner: str = None, confidential: bool = False, non_tax_deductible: bool = False, motivational_code: str = None,
                 intent_code: str = None, comment: str = None, eg_source_key: str = None, donor_partner: str = None,
                 ack_partner: str = None, pass_partner: str = None, receipt_number: str = None, 
                 received_date: datetime = None, postmark_date: datetime = None, gift_type: GiftTypes = None):


        super().__init__()
        self.a_ledger_number = ledger
        self.a_batch_number = batch
        self.a_gift_number = gift
        self.a_split_number = split
        self.a_period = period
        self.a_fund = fund
        self.a_account_code = account_code
        self.a_amount = amount
        self.a_foreign_amount = foreign_amount
        self.a_foreign_currency = foreign_currency
        self.a_foreign_currency_exch_rate = foreign_currency_exchange_rate
        self.a_foreign_currency_date = foreign_currency_date
        self.a_recv_document_id = document_id
        self.a_account_hash = account_hash
        self.a_check_front_image = check_front_image
        self.a_check_back_image = check_back_image
        self.a_posted = posted
        self.a_posted_to_gl = posted_to_gl
        self.a_gift_admin_fee = admin_fee
        self.a_gift_admin_subtype = admin_fee_subtype
        self.a_calc_admin_fee = calc_admin_fee
        self.a_calc_admin_fee_type = calc_admin_fee_type
        self.a_calc_admin_fee_subtype = calc_admin_fee_subtype
        self.p_recip_partner_id = recip_partner
        self.a_confidential = confidential
        self.a_non_tax_deductible = non_tax_deductible
        self.a_motivational_code = motivational_code
        self.a_item_intent_code = intent_code
        self.a_comment = comment
        self.i_eg_source_key = eg_source_key
        self.p_dn_donor_partner_id = donor_partner
        self.p_dn_ack_partner_id = ack_partner
        self.p_dn_pass_partner_id = pass_partner
        self.a_dn_receipt_number = receipt_number
        self.a_dn_gift_received_date = received_date
        self.a_dn_gift_postmark_date = postmark_date
        self.a_dn_gift_type = gift_type


class EGGiftImport(GiftObject):

    def __init__(self, ledger: str, gift: str, designation: str, line_item: int, transaction: str,
                 donor: str, status: str, processor: str, donor_name: str, amount: Decimal, interval: str,
                 date: datetime, designation_name: str, donor_alt: str = None, account: str = None,
                 service: str = None, returned_status: str = None, donor_given_name: str = None,
                 donor_surname: str = None, donor_middle_name: str = None, donor_prefix: str = None,
                 donor_suffix: str = None, donor_address: str = None, donor_addr1: str = None,
                 donor_addr2: str = None, donor_addr3: str = None, donor_city: str = None,
                 donor_state: str = None, donor_postal: str = None, donor_country: str = None,
                 donor_phone: str = None, donor_phone_country: str = None, donor_phone_area: str = None,
                 donor_phone_line: str = None, donor_phone_extension: str = None, donor_email: str = None,
                 currency_foreign_amount: str = None, currency: str = None, currency_date: str = None,
                 currency_exchange_rate: str = None, payment_type: str = None, lastfour: str = None,
                 start_date: datetime = None, end_date: datetime = None, gift_count: int = None,
                 trx_date: datetime = None, settlement_date: datetime = None, receipt_desired: str = None,
                 anonymous: str = None, prayforme: str = None, designation_notes: str = None,
                 net_amount: Decimal = None, deposit_date: datetime = None, deposit: str = None,
                 contra_deposit: str = None, deposit_status: str = None, deposit_gross_amount: Decimal = None,
                 deposit_amount: Decimal = None, is_modified: int = None, is_donorfee: int = None,
                 partner: str = None, donormap_confidence: int = None, donormap_future: int = None, 
                 fund: str = None, fundmap_confidence: int = None, fundmap_future: int = None, 
                 account_code: str = None, acctmap_confidence: int = None, acctmap_future: int = None, 
                 batch: int = None, batch_fees: int = None, batch_deposit: int = None):


        super().__init__()
        self.a_ledger_number = ledger
        self.i_eg_gift_uuid = gift
        self.i_eg_desig_uuid = designation
        self.i_eg_line_item = line_item
        self.i_eg_trx_uuid = transaction
        self.i_eg_donor_uuid = donor
        self.i_eg_donor_alt_id = donor_alt
        self.i_eg_account_uuid = account
        self.i_eg_service = service
        self.i_eg_status = status
        self.i_eg_returned_status = returned_status
        self.i_eg_processor = processor
        self.i_eg_donor_name = donor_name
        self.i_eg_donor_given_name = donor_given_name
        self.i_eg_donor_surname = donor_surname
        self.i_eg_donor_middle_name = donor_middle_name
        self.i_eg_donor_prefix = donor_prefix
        self.i_eg_donor_suffix = donor_suffix
        self.i_ge_donor_address = donor_address
        self.i_eg_donor_addr1 = donor_addr1
        self.i_eg_donor_addr2 = donor_addr2
        self.i_eg_donor_addr3 = donor_addr3
        self.i_eg_donor_city = donor_city
        self.i_eg_donor_state = donor_state
        self.i_eg_donor_postal = donor_postal
        self.i_eg_donor_country = donor_country
        self.i_eg_donor_phone = donor_phone
        self.i_eg_donor_phone_country = donor_phone_country
        self.i_eg_donor_phone_area = donor_phone_area
        self.i_eg_donor_phone_line = donor_phone_line
        self.i_eg_donor_phone_ext = donor_phone_extension
        self.i_eg_donor_email = donor_email
        self.i_eg_gift_amount = amount
        self.i_eg_gift_currency_foreign_amt = currency_foreign_amount
        self.i_eg_gift_currency = currency
        self.i_eg_gift_currency_date = currency_date
        self.i_eg_gift_currency_exch_rate = currency_exchange_rate
        self.i_eg_gift_pmt_type = payment_type
        self.i_eg_gift_lastfour = lastfour
        self.i_eg_gift_interval = interval
        self.i_eg_gift_start_date = start_date
        self.i_eg_gift_end_date = end_date
        self.i_eg_gift_count = gift_count
        self.i_eg_gift_date = date
        self.i_eg_gift_trx_date = trx_date
        self.i_eg_gift_settlement_date = settlement_date
        self.i_eg_receipt_desired = receipt_desired
        self.i_eg_anonymous = anonymous
        self.i_eg_prayforme = prayforme
        self.i_eg_desig_name = designation_name
        self.i_eg_desig_notes = designation_notes
        self.i_eg_net_amount = net_amount
        self.i_eg_deposit_date = deposit_date
        self.i_eg_deposit_uuid = deposit
        self.i_eg_contra_deposit_uuid = contra_deposit
        self.i_eg_deposit_status = deposit_status
        self.i_eg_deposit_gross_amt = deposit_gross_amount
        self.i_eg_deposit_amt = deposit_amount
        self.i_eg_is_modified = is_modified
        self.i_eg_is_donorfee = is_donorfee
        self.p_donor_partner_key = partner
        self.i_eg_donormap_confidence = donormap_confidence
        self.i_eg_donormap_future = donormap_future
        self.a_fund = fund
        self.i_eg_fundmap_confidence = fundmap_confidence
        self.i_eg_fundmap_future = fundmap_future
        self.a_account_code = account_code
        self.i_eg_acctmap_confidence = acctmap_confidence
        self.i_eg_acctmap_future = acctmap_future
        self.a_batch_number = batch
        self.a_batch_number_fees = batch_fees
        self.a_batch_number_deposit = batch_deposit