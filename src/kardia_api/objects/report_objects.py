from .api_object import APIObject
from datetime import datetime
from enum import Enum


class ReportObject(APIObject):

    def remove_unused_attributes(self, attributes: dict) -> dict:
        attrs_to_serialize = dict(attributes)
        del attrs_to_serialize["s_created_by"]
        del attrs_to_serialize["s_modified_by"]
        del attrs_to_serialize["s_date_created"]
        del attrs_to_serialize["s_date_modified"]
        del attrs_to_serialize["__cx_osml_control"]
        del attrs_to_serialize["required_attrs"]
        return attrs_to_serialize


class SchedStatusTypes(Enum):
        NOT_SENT = "N"
        SENT = "S"
        TEMPORARY_ERROR = "T"
        INVALID_EMAIL_ERROR = "I"
        SKIPPED = "X"
        FAILURE_OTHER_ERROR = "F"


class SchedReportStatus(ReportObject):

    def __init__(self, sent_status: SchedStatusTypes, sent_error: str, sent_date: datetime, generated_report_path: str):
        super().__init__()
        self.sent_status = sent_status.value
        self.sent_error = sent_error
        self.sent_date = sent_date if sent_date else datetime.now()
        self.generated_report_path = generated_report_path

    def serialize(self) -> dict:
        return self.remove_unused_attributes(self.__dict__)


class SchedReportBatchStatus(ReportObject):

    def __init__(self, sent_status: SchedStatusTypes = None, sent_by: str = None):
        super().__init__()
        if sent_status is not None:
            self.sent_status = sent_status.value
        if sent_by is not None:
            self.sent_by = sent_by

    def serialize(self) -> dict:
        return self.remove_unused_attributes(self.__dict__)
