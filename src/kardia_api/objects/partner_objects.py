from datetime import datetime
from enum import Enum
from .api_object import APIObject


class PartnerObject(APIObject):
    pass


class Partner(PartnerObject):

    class StatusCodes(Enum):
        """List of Kardia partner status codes
        """
        ACTIVE = "A"
        DECEASED = "D"
        OBSOLETE = "O"
        REMOVED = "X"

    class Gender(Enum):
        """List of Kardia partner genders
        """
        COUPLE = "C"
        FEMALE = "F"
        MALE = "M"

    class RecordStatusCodes(Enum):
        """List of Kardia partner record status codes
        """
        ACTIVE = "A"
        OBSOLETE = "O"
        MERGED = "M"

    class NoMailReasons(Enum):
        """List of Kardia "no mail" reasons
        """
        DECEASED = "D"
        INACTIVE_DONOR = "I"
        MISSIONARY_REQUEST = "M"
        OFFICE_REQUEST = "O"
        PERSONAL_REQUEST = "P"
        TEMPORARILY_AWAY = "T"
        UNDELIVERABLE = "U"
        OTHER = "X"

    class AcquisitionCodes(Enum):
        """List of Kardia partner acquisition codes
        """
        DATA_CONVERSION = "CNV"
        NEW_DONOR = "DON"
        MET_AT_AN_EVENT = "EVT"
        NEW_MISSIONARY = "MIS"
        OTHER = "OTH"
        MISSIONARY_PARTNER = "PAR"
        NEW_PAYEE = "PAY"
        MET_AT_RANDOM_DIVINE_APPOINTMENT = "RAN"
        NEW_STAFF_MEMBER = "STA"
        MET_AT_UNIVERSITY = "UNI"

    def __init__(self, key: str, office: str, parent_key: str = None, p_class: str = "IND", status_code: StatusCodes = StatusCodes.ACTIVE,
                 status_date: datetime = None, p_title: str = None, first_name: str = None, preferred_name: str = None, last_name: str = None,
                 last_name_first: bool = False, localized_name: str = None, suffix: str = None, org_name: str = None,
                 gender: str = None, language: str = None, acquisition: str = None, comments: str = None,
                 record_status_code: RecordStatusCodes = RecordStatusCodes.ACTIVE, no_mail_reason: str = None,
                 no_solicitations: bool = False, no_mail: bool = False, fund: str = None, best_contact: str = None,
                 merged_with: str = None, legacy_key_1: str = None, legacy_key_2: str = None, legacy_key_3: str = None,
                 staff_object = None, address_objects = None, contact_info_objects = None):

        super().__init__()
        self.p_partner_key = key
        self.p_creating_office = office
        self.p_parent_key = parent_key
        self.p_partner_class = p_class
        self.p_status_code = status_code
        self.p_status_change_date = status_date
        self.p_title = p_title
        self.p_given_name = first_name
        self.p_preferred_name = preferred_name
        self.p_surname = last_name
        self.p_surname_first = last_name_first
        self.p_localized_name = localized_name
        self.p_suffix = suffix
        self.p_org_name = org_name
        self.p_gender = gender
        self.p_language_code = language
        self.p_acquisition_code = acquisition
        self.p_comments = comments
        self.p_record_status_code = record_status_code
        self.p_no_mail_reason = no_mail_reason
        self.p_no_solicitations = no_solicitations
        self.p_no_mail = no_mail
        self.a_fund = fund
        self.p_best_contact = best_contact
        self.p_merged_with = merged_with
        self.p_legacy_key_1 = legacy_key_1
        self.p_legacy_key_2 = legacy_key_2
        self.p_legacy_key_3 = legacy_key_3
        self.staff_object = staff_object
        self.address_objects = address_objects
        self.contact_info_objects = contact_info_objects


class Address(PartnerObject):

    class LocationTypes(Enum):
        """List of Kardia address location types
        """
        HOME = "H"
        WORK = "W"
        SCHOOL = "S"
        VACATION = "V"

    class PostalModes(Enum):
        """List of Kardia address postal modes
        """
        BULK = "B"
        FIRST_CLASS = "F"

    class PostalStatuses(Enum):
        """List of Kardia address postal statuses
        """
        ADDRESSEE_UNKNOWN = "K"
        FORWARDING_ORDER_EXPIRED = "F"
        NO_SUCH_NUMBER_OR_ADDRESS = "N"
        UNDELIVERABLE = "U"
        OTHER = "X"

    class RecordStatusCodes(Enum):
        """List of Kardia address record status codes
        """
        ACTIVE = "A"
        OBSOLETE = "O"
        ACTIVE_BUT_NEEDS_QA = "Q"

    def __init__(self, key: str, location_id: int, revision_id: int, location_type: str = None, date_effective: datetime = None,
                 date_good_until: datetime = None, purge_date: datetime = None, in_care_of: str = None,
                 addr1: str = None, addr2: str = None, addr3: str = None, city: str = None, state_province: str = None,
                 country: str = None, postal_code: str = None, postal_mode: str = None, bulk_postal_code: str = None,
                 certified_date: datetime = None, postal_status: str = None, postal_barcode: str = None,
                 record_status_code: str = RecordStatusCodes.ACTIVE, comments: str = None):

        super().__init__()
        self.p_partner_key = key
        self.p_location_id = location_id
        self.p_revision_id = revision_id
        self.p_location_type = location_type
        self.p_date_effective = date_effective
        self.p_date_good_until = date_good_until
        self.p_purge_date = purge_date
        self.p_in_care_of = in_care_of
        self.p_address_1 = addr1
        self.p_address_2 = addr2
        self.p_address_3 = addr3
        self.p_city = city
        self.p_state_province = state_province
        self.p_country_code = country
        self.p_postal_code = postal_code
        self.p_postal_mode = postal_mode
        self.p_bulk_postal_code = bulk_postal_code
        self.p_certified_date = certified_date
        self.p_postal_status = postal_status
        self.p_postal_barcode = postal_barcode
        self.p_record_status_code = record_status_code
        self.p_location_comments = comments


class ContactInfo(PartnerObject):

    class ContactTypes(Enum):
        """List of Kardia contact info types
        """
        CELL = "C"
        EMAIL = "E"
        FAX = "F"
        PHONE = "P"
        WEBSITE = "W"

    class RecordStatusCodes(Enum):
        """List of Kardia contact info record status codes
        """
        ACTIVE = "A"
        OBSOLETE = "O"

    def __init__(self, key: str, contact_id: int, contact_type: str, location_id: str = None, phone_country: str = None,
                 phone_area_city: str = None, contact_data: str = None, record_status_code: str = RecordStatusCodes.ACTIVE, comments: str = None):

        super().__init__()
        self.p_partner_key = key
        self.p_contact_id = contact_id
        self.p_contact_type = contact_type
        self.p_location_id = location_id
        self.p_phone_country = phone_country
        self.p_phone_area_city = phone_area_city
        self.p_contact_data = contact_data
        self.p_record_status_code = record_status_code
        self.p_comments = comments


class Staff(PartnerObject):

    def __init__(self, key: str, is_staff: bool = None, kardia_login: str = None, kardiaweb_login: str = None,
                 preferred_email: int = None, preferred_location: int = None):

        super().__init__()
        self.p_partner_key = key
        self.p_is_staff = is_staff
        self.p_kardia_login = kardia_login
        self.p_kardiaweb_login = kardiaweb_login
        self.p_preferred_email_id = preferred_email
        self.p_preferred_location_id = preferred_location