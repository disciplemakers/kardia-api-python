import inspect
import re
from modules.donor import Donor
from modules.crm_config import CRM_Config
from modules.disbursements import Disbursements
import os
os.chdir(os.path.dirname(os.path.abspath(__file__)))

modules = [
    CRM_Config,
    Donor
]

for module in modules:

    try:
        template_file = open(
            "tests/test_{}.py".format(module.__name__.lower()))
        template = template_file.read()
    except Exception as e:
        template_file = open("tests/test_template.py")
        template = template_file.read().replace(
            "TestClass", "Test{}".format(module.__name__))
        template = template.replace("module", module.__name__.lower())

    for f in inspect.getmembers(module, inspect.isfunction):
        if re.search(r"^get\w|^create\w", f[0]):
            args = ", ".join(
                "self." + x for x in (inspect.getfullargspec(f[1]).args[1:]))
            f = f[0]
            if not re.search(f, template):
                print("Adding test_{}".format(f))
                template += '\n\n    def test_{}(self):'.format(f)
                template += '\n        #TODO'
                template += '\n        self.doTest(self.func({}), val="TODO", js_key="TODO")'.format(
                    args)

    template_file.close()

    with open("tests/test_{}.py".format(module.__name__.lower()), "w+") as test_file:
        test_file.write(template)
