from requests.models import Response
from .api_module import APIModule
from enum import Enum

# TODO getPartnerComments
# TODO Split out module endpoint to own file


class CRM(APIModule):

    # ------------------------------------ Endpoints ------------------------------------

    class ModuleEndpoint(Enum):
        CRM = "crm"
        CRM_CONFIG = "crm_config"
        MISSIONARY = "missionary"
        SUPPORTER = "supporter"

    class CRMEndpoint(Enum):
        PARTNERS = "Partners"

    class PartnerEndpoint(Enum):
        TAGS = "Tags"
        DOCUMENTS = "Documents"
        CONTACT_HISTORY = "ContactHistory"
        CONTACT_AUTORECORD = "ContactAutorecord"
        TRACKS = "Tracks"
        COLLABORATORS = "Collaborators"
        COLLABORATEES = "Collaboratees"
        COLLABORATOR_TODOS = "CollaboratorTodos"
        TODOS = "Todos"
        WORKFLOWS = "Workflows"
        DATA_GROUPS = "DataGroups"
        DATA_ITEMS = "DataItems"
        ACTIVITY = "Activity"
        COMMENTS = "Comments"

    class ConfigEndpoint(Enum):
        TRACKS = "Tracks"
        TAG_TYPES = "TagTypes"
        CONTACT_HIST_TYPES = "ContactHistTypes"
        COLLABORATOR_TYPES = "CollaboratorTypes"
        TODO_TYPES = "TodoTypes"
        WORKFLOW_TYPES = "WorkflowTypes"
        DATA_ITEM_TYPES = "DataItemTypes"
        DOCUMENT_TYPES = "DocumentTypes"
        COUNTRIES = "Countries"
        TEXT_EXPANSIONS = "TextExpansions"

    class TrackEndpoint(Enum):
        STEP = "Steps"
        COLLABORATORS = "Collaborators"

    class StepEndpoint(Enum):
        COLLABORATORS = "Collaborators"

    class SupporterEndpoint(Enum):
        PRAYERS = "Prayers"
        COMMENTS = "Comments"
        MISSIONARIES = "Missionaries"

# ------------------------------------ Getters ------------------------------------

    def _getModuleEndpoint(self, data: ModuleEndpoint, value: str = "", collection: bool = False) -> Response:
        self.endpoint = "/{}/{}/{}".format(data.value, value, self.endpoint)
        if collection:
            self.res_type = "collection"
        else:
            self.res_format = "attrs"
        return self.get()

    def _getConfigEndpoint(self, data: ConfigEndpoint, value: str = "", collection: bool = False) -> Response:
        self.endpoint = "{}/{}/{}".format(
            data.value, value, self.endpoint)
        return self._getModuleEndpoint(self.ModuleEndpoint.CRM_CONFIG, collection=not value or collection)

    def _getTrackEndpoint(self, track: str, data: TrackEndpoint, value: str = "", collection: bool = False) -> Response:
        self.endpoint = "{}/{}/{}".format(data.value, value, self.endpoint)
        return self._getConfigEndpoint(self.ConfigEndpoint.TRACKS, track, collection=not value or collection)

    def _getTrackStepEndpoint(self, track: str, step: str, data: StepEndpoint, value: str = "") -> Response:
        self.endpoint = "{}/{}/{}".format(data.value, value, self.endpoint)
        return self._getTrackEndpoint(track, self.TrackEndpoint.STEP, step, not value)

    def _getCRMEndpoint(self, data: CRMEndpoint, value: str = "", collection: bool = False) -> Response:
        self.endpoint = "{}/{}/{}".format(
            data.value, value, self.endpoint)
        return self._getModuleEndpoint(self.ModuleEndpoint.CRM, collection=not value or collection)

    def _getPartnerEndpoint(self, partner_id: str, data: PartnerEndpoint, value: str = "") -> Response:
        self.endpoint = "{}/{}/{}".format(data.value, value, self.endpoint)
        return self._getCRMEndpoint(self.CRMEndpoint.PARTNERS, partner_id, not value)

    def _getSupporterEndpoint(self, supporter_id: str, data: SupporterEndpoint, value: str = "") -> Response:
        self.endpoint = "{}/{}/{}".format(data.value, value, self.endpoint)
        return self._getModuleEndpoint(self.ModuleEndpoint.SUPPORTER, supporter_id, not value)

    def getTracks(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TRACKS)

    def getTrack(self, track: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TRACKS, track)

    def getTrackSteps(self, track: str) -> Response:
        return self._getTrackEndpoint(track, self.TrackEndpoint.STEP)

    def getTrackStep(self, track: str, step: str) -> Response:
        return self._getTrackEndpoint(track, self.TrackEndpoint.STEP, step)

    def getStepCollaborators(self, track: str, step: str) -> Response:
        return self._getTrackStepEndpoint(track, step, self.StepEndpoint.COLLABORATORS)

    def getStepCollaborator(self, track: str, step: str, track_id: int, step_id: int, collaborator_id: str) -> Response:
        return self._getTrackStepEndpoint(track, step, self.StepEndpoint.COLLABORATORS, "{}|{}|{}".format(track_id, step_id, collaborator_id))

    def getTrackCollaborators(self, track: str) -> Response:
        return self._getTrackEndpoint(track, self.TrackEndpoint.COLLABORATORS)

    def getTrackCollaborator(self, track: str, track_id: int, collaborator_id: str) -> Response:
        return self._getTrackEndpoint(track, self.TrackEndpoint.COLLABORATORS, "{}|{}".format(track_id, collaborator_id))

    def getTagTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TAG_TYPES)

    def getTagType(self, tag_type_id: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TAG_TYPES, tag_type_id)

    def getContactHistTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.CONTACT_HIST_TYPES)

    def getContactHistType(self, contact_history_type_id: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.CONTACT_HIST_TYPES, contact_history_type_id)

    def getCollaboratorTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.COLLABORATOR_TYPES)

    def getCollaboratorType(self, collab_type_id: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.COLLABORATOR_TYPES, collab_type_id)

    def getTodoTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TODO_TYPES)

    def getTodoType(self, todo_type_id: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TODO_TYPES, todo_type_id)

    def getWorkflowTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.WORKFLOW_TYPES)

    def getDataItemTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.DATA_ITEM_TYPES)

    def getDataItemType(self, di_type_id: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.DATA_ITEM_TYPES, di_type_id)

    def getDocumentTypes(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.DOCUMENT_TYPES)

    def getDocumentType(self, doc_type_id: str) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.DOCUMENT_TYPES, doc_type_id)

    def getCountries(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.COUNTRIES)

    def getCountry(self, country: str) -> Response:
        # TODO Currently broken in the API
        return self._getConfigEndpoint(self.ConfigEndpoint.COUNTRIES, country)

    def getTextExpansions(self) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TEXT_EXPANSIONS)

    def getTextExpansion(self, expansion_id) -> Response:
        return self._getConfigEndpoint(self.ConfigEndpoint.TEXT_EXPANSIONS, expansion_id)

    def getPartners(self) -> Response:
        return self._getCRMEndpoint(self.CRMEndpoint.PARTNERS)

    def getPartner(self, partner_id) -> Response:
        return self._getCRMEndpoint(self.CRMEndpoint.PARTNERS, partner_id)

    def getPartnerTags(self, partner_id: str) -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.TAGS)

    def getPartnerTag(self, partner_id: str, tag: str) -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.TAGS, tag)

    def getPartnerDocuments(self, partner_id: str) -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.DOCUMENTS)

    def getPartnerDocument(self, partner_id: str, document: str) -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.DOCUMENTS, document)

    def getPartnerContactHistory(self, partner_id: str, contact_history_id: str = "") -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.CONTACT_HISTORY, contact_history_id)

    def getPartnerContactAutorecord(self, partner_id: str, contact_history_id: str = "") -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.CONTACT_AUTORECORD, contact_history_id)

    def getPartnerTracks(self, partner_id: str) -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.TRACKS)

    def getPartnerTrack(self, partner_id: str, track: str) -> Response:
        return self._getPartnerEndpoint(partner_id, self.PartnerEndpoint.TRACKS, track)

    def getSupporters(self) -> Response:
        return self._getModuleEndpoint(self.ModuleEndpoint.SUPPORTER, collection=True)

    def getSupporter(self, supporter_id: str) -> Response:
        return self._getModuleEndpoint(self.ModuleEndpoint.SUPPORTER, supporter_id)

    def getSupporterPrayers(self, supporter_id: str) -> Response:
        return self._getSupporterEndpoint(supporter_id, self.SupporterEndpoint.PRAYERS)

    def getSupporterPrayer(self, supporter_id: str, prayer_id: str) -> Response:
        return self._getSupporterEndpoint(supporter_id, self.SupporterEndpoint.PRAYERS, prayer_id)

    def getSupporterComments(self, supporter_id: str) -> Response:
        return self._getSupporterEndpoint(supporter_id, self.SupporterEndpoint.COMMENTS)

    def getSupporterComment(self, supporter_id: str, comment_id: str) -> Response:
        return self._getSupporterEndpoint(supporter_id, self.SupporterEndpoint.COMMENTS, comment_id)

    def getSupporterMissionaries(self, supporter_id: str) -> Response:
        return self._getSupporterEndpoint(supporter_id, self.SupporterEndpoint.MISSIONARIES)

    def getSupporterMissionary(self, supporter_id: str, missionary_id) -> Response:
        return self._getSupporterEndpoint(supporter_id, self.SupporterEndpoint.MISSIONARIES, missionary_id)

    def getMissionaries(self) -> Response:
        return self._getModuleEndpoint(self.ModuleEndpoint.MISSIONARY, collection=True)

    def getMissionary(self, missionary_id) -> Response:
        return self._getModuleEndpoint(self.ModuleEndpoint.MISSIONARY, missionary_id)

# ------------------------------------ CRM Objects ------------------------------------
# ------------------------------------ Creaters ------------------------------------
