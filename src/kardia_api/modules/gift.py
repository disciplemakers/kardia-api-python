from .api_module import APIModule
from datetime import datetime
from decimal import Decimal
from enum import Enum
from .gl import GL
from ..objects.gift_objects import Gift, GiftItem, EGGiftImport
from ..objects.gl_objects import Batch
import requests
from requests.models import Response
from typing import List


class Endpoints(Enum):
    PARENT = None
    LEDGERS = "Ledgers"


class LedgerEndpoints(Enum):
    PARENT = Endpoints.LEDGERS
    BATCHES = "Batches"
    NEXT_RECEIPT_NUMBER = "NextReceiptNumber"
    EG_GIFT_IMPORTS = "EGGiftImports"


class BatchEndpoints(Enum):
    PARENT = LedgerEndpoints.BATCHES
    GIFTS = "Gifts"


class GiftEndpoints(Enum):
    PARENT = BatchEndpoints.GIFTS
    ITEMS = "Items"


class Gift(APIModule):

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

    def getLedger(self, ledger_id: str) -> Response:
        return self._getEndpoint(Endpoints.LEDGERS, ledger_id)

    def getLedgers(self):
        return self._getEndpoint(Endpoints.LEDGERS)

    def getBatch(self, ledger_id: str, batch_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        return self._getEndpoint(LedgerEndpoints.BATCHES, ledger_id, batch_and_ledger_id)

    def getBatches(self, ledger_id: str) -> Response:
        return self._getEndpoint(LedgerEndpoints.BATCHES, ledger_id)

    def createBatch(self, batch: Batch, ledger_id: str) -> Response:
        if not batch.a_batch_number:
            gl = GL(kardia_url=self.kardia_url, user=self.user, pw=self.pw)
            batch.a_batch_number = gl.getNextBatchId(batch.a_ledger_number).json()["response"]["batch_number"]
        self.payload = batch.serialize()
        return self._postEndpoint(LedgerEndpoints.BATCHES, ledger_id)

    def getGift(self, ledger_id: str, batch_id: int, gift_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        gift_and_batch_and_ledger_id = "{}|{}|{}".format(ledger_id, batch_id, gift_id)
        return self._getEndpoint(BatchEndpoints.GIFTS, ledger_id, batch_and_ledger_id, gift_and_batch_and_ledger_id)

    def getGifts(self, ledger_id: str, batch_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        return self._getEndpoint(BatchEndpoints.GIFTS, ledger_id, batch_and_ledger_id)

    def createGift(self, gift: Gift, ledger_id: str, batch_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        self.payload = gift.serialize()
        return self._postEndpoint(BatchEndpoints.GIFTS, ledger_id, batch_and_ledger_id)

    def getGiftItem(self, ledger_id: str, batch_id: str, gift_id: int, item_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        gift_and_batch_and_ledger_id = "{}|{}|{}".format(ledger_id, batch_id, gift_id)
        item_and_gift_and_batch_and_ledger_id = "{}|{}|{}|{}".format(ledger_id, batch_id, item_id, gift_id)
        return self._getEndpoint(GiftEndpoints.ITEMS, ledger_id, batch_and_ledger_id, gift_and_batch_and_ledger_id, item_and_gift_and_batch_and_ledger_id)

    def getGiftItems(self, ledger_id: str, batch_id: int, gift_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        gift_and_batch_and_ledger_id = "{}|{}|{}".format(ledger_id, batch_id, gift_id)
        return self._getEndpoint(GiftEndpoints.ITEMS, ledger_id, batch_and_ledger_id, gift_and_batch_and_ledger_id)

    def createGiftItem(self, item: GiftItem, ledger_id: str, batch_id: int, gift_id: int) -> Response:
        batch_and_ledger_id = "{}|{}".format(batch_id, ledger_id)
        gift_and_batch_and_ledger_id = "{}|{}|{}".format(ledger_id, batch_id, gift_id)
        self.payload = item.serialize()
        return self._postEndpoint(GiftEndpoints.ITEMS, ledger_id, batch_and_ledger_id, gift_and_batch_and_ledger_id)


    def createBatchWithGifts(self, ledger: str, period: str, gifts: List[Gift], desc: str = None, date: datetime = None, posted: bool = False, posted_to_gl: bool = False) -> dict:
        responses = { 'Batch': None,
                      'Gifts': [],
                    }
        batch = Batch(ledger, period, desc=desc, origin="CR", date=date if date else datetime.now())
        res = self.createBatch(batch, ledger)
        responses['Batch'] = res
        if res.status_code == requests.codes.created:
            batch_id = res.json()["a_batch_number"]["v"]
        else:
            return responses

        # Make sure that each gift has all the required fields filled out
        for gift_counter, gift in enumerate(gifts, 1):
            if not gift.a_ledger_number:
                gift.a_ledger_number = ledger
            if not gift.a_batch_number:
                gift.a_batch_number = batch_id
            if not gift.a_gift_number:
                gift.a_gift_number = gift_counter
            if not gift.a_period:
                gift.a_period = period
            if not gift.a_amount:
                gift.a_amount = Decimal("0.00")
                for item in gift.item_objects:
                    gift.a_amount += item.a_amount
            gift.a_posted = posted
            gift.a_posted_to_gl = posted_to_gl
            if not gift.a_receipt_number:
                gift.a_receipt_number = str(self.getNextReceiptNumber(gift.a_ledger_number).json()["response"]["receipt_number"])
            if not gift.a_gift_received_date:
                gift.a_gift_received_date = date
            if gift.a_receipt_sent and not gift.a_receipt_sent_date:
                gift.a_receipt_sent_date = date
            if gift.a_ack_receipt_sent and not gift.a_ack_receipt_sent_date:
                gift.a_ack_receipt_sent_date = date

            res = self.createGift(gift, gift.a_ledger_number, gift.a_batch_number)
            gift_response = { 'Gift': res }
            item_responses = []

            # Make sure that each gift item has all the required fields filled out
            for item_counter, item in enumerate(gift.item_objects, 1):
                if not item.a_ledger_number:
                    item.a_ledger_number = ledger
                if not item.a_batch_number:
                    item.a_batch_number = batch_id
                if not item.a_gift_number:
                    item.a_gift_number = gift_counter
                if not item.a_split_number:
                    item.a_split_number = item_counter
                if not item.a_period:
                    item.a_period = period
                if item.a_check_front_image == "LEGACY_RECEIPT_NUM":
                    item.a_check_front_image = "check-legacy-{}.png".format(gift.a_receipt_number)
                if not item.a_posted:
                    item.a_posted = posted
                if not item.a_posted_to_gl:
                    item.a_posted_to_gl = posted_to_gl
                if not item.p_dn_donor_partner_id:
                    item.p_dn_donor_partner_id = gift.p_donor_partner_id
                if not item.p_dn_ack_partner_id:
                    item.p_dn_ack_partner_id = gift.p_ack_partner_id
                if not item.p_dn_pass_partner_id:
                    item.p_dn_pass_partner_id = gift.p_pass_partner_id
                if not item.a_dn_receipt_number:
                    item.a_dn_receipt_number = gift.a_receipt_number
                if not item.a_dn_gift_received_date:
                    item.a_dn_gift_received_date = gift.a_gift_received_date
                if not item.a_dn_gift_postmark_date:
                    item.a_dn_gift_postmark_date = gift.a_gift_postmark_date
                if not item.a_dn_gift_type:
                    item.a_dn_gift_type = gift.a_gift_type

                res = self.createGiftItem(item, gift.a_ledger_number, gift.a_batch_number, gift.a_gift_number)
                item_responses.append(res)

            gift_response['Items'] = item_responses
            responses['Gifts'].append(gift_response)

        return responses

    def getNextReceiptNumber(self, ledger_id: str) -> Response:
        self.res_attrs = "basic"
        return self._getEndpoint(LedgerEndpoints.NEXT_RECEIPT_NUMBER, ledger_id)

    def createEGGiftImport(self, eg_gift_import: EGGiftImport, ledger_id: str) -> Response:
        #ledger_and_transaction_and_designation_id = "{}|{}|{}".format(ledger_id, transaction_id, designation_id)
        self.payload = eg_gift_import.serialize()
        return self._postEndpoint(LedgerEndpoints.EG_GIFT_IMPORTS, ledger_id)
