from enum import Enum
from ..objects.gl_objects import Account, Batch, Fund, Period, Transaction, Year, AnalysisAttribute, AnalysisAttributeValue, AccountAnalysisAttribute, FundAnalysisAttribute
from requests.models import Response
from .api_module import APIModule
from datetime import datetime, timedelta
from decimal import Decimal
from typing import List


class Default(Enum):
    PARENT = None
    LEDGER_ID = "/"  # TODO move this to api_module.py


class Endpoints(Enum):
    PARENT = Default.LEDGER_ID
    ACCOUNTS = "Accounts"
    ALLFUNDS = "AllFunds"
    FUNDS = "Funds"
    NEXTBATCH = "NextBatch"
    PERIODS = "Periods"
    ANALYSIS_ATTRIBUTES = "AnalysisAttributes"
    ANALYSIS_ATTRIBUTE_VALUES = "AnalysisAttributeValues"
    ACCOUNT_ANALYSIS_ATTRIBUTES = "AccountAnalysisAttributes"
    FUND_ANALYSIS_ATTRIBUTES = "FundAnalysisAttributes"


class AccountEndpoints(Enum):
    PARENT = Endpoints.ACCOUNTS
    ANALYSIS_ATTRIBUTES = "AnalysisAttributes"


class FundEndpoints(Enum):
    PARENT = Endpoints.FUNDS
    SUBFUNDS = "SubFunds"
    ANALYSIS_ATTRIBUTES = "AnalysisAttributes"


class PeriodEndpoints(Enum):
    PARENT = Endpoints.PERIODS
    BATCHES = "Batches"


class BatchEndpoints(Enum):
    PARENT = PeriodEndpoints.BATCHES
    TRANSACTIONS = "Transactions"


class GL(APIModule):

    def __init__(self, **kwargs):
        """[summary]

        Args:
            config (dict): Kardia api config
        """        #
        super().__init__(**kwargs)
        self.module = self.__class__.__name__.lower()

    def getAccounts(self, ledger_id: str) -> Response:
        """Gets all accounts for a given leger

        Args:
            ledger_id (str): Leger to get accounts for

        Returns:
            Response: Responses from the api.
        """
        return self._getEndpoint(Endpoints.ACCOUNTS, ledger_id)

    def getAccount(self, ledger_id: str, account_id: str):
        """Gets a specific account for a given ledger.

        Args:
            ledger_id (str): Ledger that conaints the account.
            account_id (str): Account to get.

        Returns:
            Response: Response from the api.
        """
        account_id = "{}|{}".format(account_id, ledger_id)
        return self._getEndpoint(Endpoints.ACCOUNTS, ledger_id, account_id)

    def createAccount(self, account: Account):
        self.payload = account.serialize()
        return self._postEndpoint(Endpoints.ACCOUNTS, account.a_ledger_number)

    def getAllFunds(self, ledger_id):
        return self._getEndpoint(Endpoints.ALLFUNDS, ledger_id)

    def getFunds(self, ledger_id):
        return self._getEndpoint(Endpoints.FUNDS, ledger_id)

    def getFund(self, ledger_id, fund_id):
        fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(Endpoints.FUNDS, ledger_id, fund_id)

    def createFund(self, fund: Fund):
        self.payload = fund.serialize()
        return self._postEndpoint(Endpoints.FUNDS, fund.a_ledger_number)

    def getSubfunds(self, ledger_id, fund_id):
        fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.SUBFUNDS, ledger_id, fund_id)

    def getSubfund(self, ledger_id, fund_id, subfund_id):
        fund_id = "{}|{}".format(fund_id, ledger_id)
        subfund_id = "{}|{}".format(subfund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.SUBFUNDS, ledger_id, fund_id, subfund_id)

    def createSubFund(self, fund: Fund):
        self.payload = fund.serialize()
        parent_fund_id = "{}|{}".format(fund.a_parent_fund, fund.a_ledger_number)
        return self._postEndpoint(FundEndpoints.SUBFUNDS, fund.a_ledger_number, parent_fund_id)


    def getBatches(self, ledger_id, year_id, period_id):
        period_id = "{}|{}/{}|{}".format(year_id,
                                         ledger_id, period_id, ledger_id)
        return self._getEndpoint(PeriodEndpoints.BATCHES, ledger_id, period_id)

    def getBatch(self, ledger_id, year_id, period_id, batch_id):
        period_id = "{}|{}/{}|{}".format(year_id,
                                         ledger_id, period_id, ledger_id)
        batch_id = "{}|{}".format(batch_id, ledger_id)
        return self._getEndpoint(PeriodEndpoints.BATCHES, ledger_id, period_id, batch_id)

    def getNextBatchId(self, ledger_id) -> int:
        self.res_attrs = "basic"  # handle in api_module
        return self._getEndpoint(Endpoints.NEXTBATCH, ledger_id)

    def createBatch(self, year_id, batch: Batch):
        if not batch.a_batch_number:
            batch.a_batch_number = self.getNextBatchId(batch.a_ledger_number).json()["response"]["batch_number"]
        period_id = "{}|{}/{}|{}".format(year_id, batch.a_ledger_number,
                                         batch.a_period, batch.a_ledger_number)
        self.payload = batch.serialize()
        return self._postEndpoint(PeriodEndpoints.BATCHES, batch.a_ledger_number, period_id)

    def getPeriods(self, ledger_id, year_id):
        year_id = "{}|{}".format(year_id, ledger_id)
        self.res_type = "collection"  # TODO move this to apy_module.py
        return self._getEndpoint(Endpoints.PERIODS, ledger_id, year_id)

    def getPeriod(self, ledger_id, year_id, period_id):
        year_id = "{}|{}".format(year_id, ledger_id)
        period_id = "{}/{}|{}".format(year_id, period_id, ledger_id)
        return self._getEndpoint(Endpoints.PERIODS, ledger_id, period_id)

    def createPeriod(self, period: Period):
        # TODO handled getting top level parent (year > quarter > ... > months > etc)
        period.checkValid()
        endpoint = "/gl/{}/Periods".format(period.a_ledger_number)
        parent = period.serialize()
        while (parent["a_parent_period"]):
            res = self.getYear(parent["a_ledger_number"],
                               parent["a_parent_period"])
            if (res.status_code == 200):
                parent = res.json()
                endpoint += "/{}|{}".format(parent["a_period"],
                                            parent["a_ledger_number"])
            else:
                msg = "Unable to find {}.\nNote: Kardia does not currently support children periods also being parent periods."
                raise Exception(msg.format(res.url))
        self.payload = period.serialize()
        self.res_type = "collection"
        self.endpoint = endpoint
        return self.post()

    def getYears(self, ledger_id: str):
        return self._getEndpoint(Endpoints.PERIODS, ledger_id)

    def getYear(self, ledger_id: str, year_id: str):
        year_id = "{}|{}".format(year_id, ledger_id)
        return self._getEndpoint(Endpoints.PERIODS, ledger_id, year_id)

    def createYear(self, year: Year):
        self.payload = year.serialize()
        return self._postEndpoint(Endpoints.PERIODS, year.a_ledger_number)

    def createMonth(self, ledger_id: str, year_id: str, month: int, month_id: str = None, status: str = Period.PeriodStatuses.NEVER_OPENED):
        res = self.getYear(ledger_id, year_id)  # TODO handle failed GET
        if res.status_code != 200:
            return res
        year = res.json()
        y_start_date = year["a_start_date"]
        y_end_date = year["a_end_date"]
        y_start_date_obj = datetime(y_start_date["year"], y_start_date["month"], y_start_date["day"])
        y_end_date_obj = datetime(y_end_date["year"], y_end_date["month"], y_end_date["day"])
        # Verify that the specified month exists in this fiscal year
        valid_months = []
        cur_date = y_start_date_obj
        while cur_date <= y_end_date_obj:
            if cur_date.month in valid_months:
                # This method only works with years that are 12 months or less, so a given month number shouldn't exist more than once in the fiscal year
                raise Exception("createMonth() only works for fiscal years that are 12 months long or less.")
            else:
                valid_months.append(cur_date.month)
            if cur_date.month == 12:
                cur_date = datetime(cur_date.year + 1, 1, cur_date.day)
            else:
                cur_date = datetime(cur_date.year, cur_date.month + 1, cur_date.day)
        if month not in valid_months:
            raise Exception("Month {} does not exist is the {} year date range.".format(month, year_id))
        # Create month period
        if month < y_start_date_obj.month:
            m_year = y_start_date_obj.year + 1
        else:
            m_year = y_start_date_obj.year
        m_start_date = datetime(m_year, month, 1)
        if (month == 12):
            m_end_date = datetime(m_year, month, 31, 23, 59, 59)
        else:
            m_end_date = datetime(m_year, month+1, 1, 23, 59, 59) - timedelta(days=1)
        month_id = month_id if month_id else m_start_date.strftime("%Y.%m")
        # Set opened/closed dates for past periods (data migration)
        if m_start_date < datetime.now():
            first_opened = m_start_date
        else:
            first_opened = None
        if m_end_date < datetime.now():
            last_closed = m_end_date
        else:
            last_closed = None
        p = Period(month_id, ledger_id, m_start_date, m_end_date,
                   year_id, status, first_opened = first_opened,
                   last_closed = last_closed, desc=m_start_date.strftime("%B %Y"))
        return self.createPeriod(p)

    def getTransactions(self, ledger_id, year_id, period_id, batch_id):
        period_id = "{}|{}/{}|{}".format(year_id,
                                         ledger_id, period_id, ledger_id)
        batch_id = "{}|{}".format(batch_id, ledger_id)
        return self._getEndpoint(BatchEndpoints.TRANSACTIONS, ledger_id, period_id, batch_id)

    def createJEWithTrans(self, ledger_id, year_id, period_id, transactions: List[Transaction], journal_id=1, batch_id=None, date=None, desc="", post=True):
        """Creates a journal entry for a generated or specificed batch and adds a list of transactions to it. Each transactions should contain a(n) fund, account code, and amount.

        Args:
            ledger_id (str): Ledger in which the journal entry will be added.
            year_id (str): Year in which the journal entry will be added.
            period_id (str): Period in which the journal entry will be added.
            transactions (List[Transaction]): Transactions to be added to the journal entry.
            journal_id (int, optional): ID of the journal entry to be created. Defaults to 1.
            batch_id (str, optional): Batch to add the transactions to. Defaults to None.
            date (datetime, optional): Effective date for the transactions in this batch. Defaults to the date when called.
            desc (str, optional): Desciption of the journal entry. Defaults to "".
            post (bool, optional): Whether or not all the transactions will be posted. Defaults to True.

        Raises:
            Exception: If the batch fails to be created.

        Returns:
            List[Response]: the responses form the api for each transaction being added.
        """
        for trans in transactions:
            trans.a_ledger_number = ledger_id
            trans.a_period = period_id
            trans.a_batch_number = -1
            trans.a_journal_number = journal_id
            trans.a_posted = post.real
        responses = {
            "batch": None,
            "transactions": []
        }
        if not batch_id:
            batch = Batch(ledger_id, period_id, desc=desc, date=date)
            res = self.createBatch(year_id, batch)
            if res.status_code != 201:
                raise Exception(
                    "Failed to create new batch: \n{}".format(res.content))
            responses["batch"] = res
            batch_id = res.json()["a_batch_number"]["v"]
        for trans in transactions:
            trans.a_batch_number = batch_id
            res = self.createTransaction(year_id, trans)
            responses["transactions"].append(res)
        return responses

    def createTransaction(self, year_id: str, trans: Transaction):
        """Creates a transaction for a given year. The period and batch should supplied via the transaction.

        Args:
            year_id (str): Year the transaction is in
            trans (Transaction): Transaction to be created

        Returns:
            Response: Response from the api.
        """
        self.payload = trans.serialize()
        period_id = "{}|{}/{}|{}".format(year_id, trans.a_ledger_number,
                                         trans.a_period, trans.a_ledger_number)
        batch_id = "{}|{}".format(trans.a_batch_number, trans.a_ledger_number)
        return self._postEndpoint(BatchEndpoints.TRANSACTIONS, trans.a_ledger_number, period_id, batch_id)

    def createTransactions(self, year_id: str, transactions: List[Transaction]):
        """Creates transactions for a given year. The period and batch should supplied via each transaction.

        Args:
            year_id (str): Year the transaction is in
            transactions (List[Transaction]): Transactions to be created.

        Returns:
            List[Response]: Responses from the api for each transaction being added.
        """
        responses = []
        for trans in transactions:
            res = self.createTransaction(year_id, trans)
            responses.append(res)
        return responses

    def getTransaction(self, ledger_id: str, year_id: str, period_id: str, batch_id: int, trans_id: int, journal_id: int = 1):
        """Gets a transaction for a giveing, ledger, year, period (month), batch and optional journal entry.

        Args:
            ledger_id (str): Ledger containg the transaction
            year_id (str): Year containing the transaction
            period_id (str): Period (month) containing the transaction
            batch_id (int): Batch containing the transaction
            trans_id (int): Transaction ID
            journal_id (int, optional): Journal entry containing the transaction. Defaults to 1.

        Returns:
            Response: Responses from the api.
        """
        trans_id = "{}|{}|{}|{}".format(
            ledger_id, batch_id, journal_id, trans_id)
        period_id = "{}|{}/{}|{}".format(year_id,
                                         ledger_id, period_id, ledger_id)
        batch_id = "{}|{}".format(batch_id, ledger_id)
        return self._getEndpoint(BatchEndpoints.TRANSACTIONS, ledger_id, period_id, batch_id, trans_id)


    def getAnalysisAttributes(self, ledger_id: str) -> Response:
        return self._getEndpoint(Endpoints.ANALYSIS_ATTRIBUTES, ledger_id)

    def getAnalysisAttribute(self, ledger_id: str, attr_code: str) -> Response:
        full_attr_id = "{}|{}".format(attr_code, ledger_id)
        return self._getEndpoint(Endpoints.ANALYSIS_ATTRIBUTES, ledger_id, full_attr_id)

    def createAnalysisAttribute(self, analysis_attribute: AnalysisAttribute):
        self.payload = analysis_attribute.serialize()
        return self._postEndpoint(Endpoints.ANALYSIS_ATTRIBUTES, analysis_attribute.a_ledger_number)


    def getAnalysisAttributeValues(self, ledger_id: str) -> Response:
        return self._getEndpoint(Endpoints.ANALYSIS_ATTRIBUTE_VALUES, ledger_id)

    def getAnalysisAttributeValue(self, ledger_id: str, attr_code: str, value: str) -> Response:
        full_attr_id = "{}|{}|{}".format(attr_code, ledger_id, value)
        return self._getEndpoint(Endpoints.ANALYSIS_ATTRIBUTE_VALUES, ledger_id, full_attr_id)

    def createAnalysisAttributeValue(self, analysis_attribute_value: AnalysisAttributeValue):
        self.payload = analysis_attribute_value.serialize()
        return self._postEndpoint(Endpoints.ANALYSIS_ATTRIBUTE_VALUES, analysis_attribute_value.a_ledger_number)


    def getAccountAnalysisAttributes(self, ledger_id: str) -> Response:
        return self._getEndpoint(Endpoints.ACCOUNT_ANALYSIS_ATTRIBUTES, ledger_id)

    def getAccountAnalysisAttribute(self, ledger_id: str, account_code: str, attr_code: str) -> Response:
        full_attr_id = "{}|{}|{}".format(attr_code, ledger_id, account_code)
        return self._getEndpoint(Endpoints.ACCOUNT_ANALYSIS_ATTRIBUTES, ledger_id, full_attr_id)

    def createAccountAnalysisAttribute(self, account_analysis_attribute: AccountAnalysisAttribute):
        self.payload = account_analysis_attribute.serialize()
        return self._postEndpoint(Endpoints.ACCOUNT_ANALYSIS_ATTRIBUTES, account_analysis_attribute.a_ledger_number)

    def getAccountAccountAnalysisAttributes(self, ledger_id: str, account_code: str) -> Response:
        full_account_id = "{}|{}".format(account_code, ledger_id)
        return self._getEndpoint(AccountEndpoints.ANALYSIS_ATTRIBUTES, ledger_id, full_account_id)


    def getFundAnalysisAttributes(self, ledger_id: str) -> Response:
        return self._getEndpoint(Endpoints.FUND_ANALYSIS_ATTRIBUTES, ledger_id)

    def getFundAnalysisAttribute(self, ledger_id: str, fund_id: str, attr_code: str) -> Response:
        full_attr_id = "{}|{}|{}".format(attr_code, ledger_id, fund_id)
        return self._getEndpoint(Endpoints.FUND_ANALYSIS_ATTRIBUTES, ledger_id, full_attr_id)

    def createFundAnalysisAttribute(self, fund_analysis_attribute: FundAnalysisAttribute):
        self.payload = fund_analysis_attribute.serialize()
        return self._postEndpoint(Endpoints.FUND_ANALYSIS_ATTRIBUTES, fund_analysis_attribute.a_ledger_number)

    def getFundFundAnalysisAttributes(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.ANALYSIS_ATTRIBUTES, ledger_id, full_fund_id)

