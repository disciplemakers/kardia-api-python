from requests.models import Response
from .api_module import APIModule
from enum import Enum
from typing import Dict

# ------------------------------------ Endpoints ------------------------------------


class Endpoints(Enum):
    PARENT = None
    CONFIG = "Config"

# ------------------------------------ Module ------------------------------------


class Config(APIModule):

    # ------------------------------------ Init ------------------------------------

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

    # ------------------------------------ Getters ------------------------------------

    def getConfig(self) -> Response:
        return self._getEndpoint(Endpoints.CONFIG)

