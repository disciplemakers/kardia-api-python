from enum import Enum
from .api_module import APIModule


class Default(Enum):
    PARENT = None
    LEDGER_ID = "/"  # TODO move this to api_module.py


class Endpoints(Enum):
    PARENT = Default.LEDGER_ID
    CONFIG = "Config"


class AccountConfig(APIModule):
    def __init__(self, kardia_url, user, pw) -> None:
        super().__init__(kardia_url, user, pw)
        self.module = "acctconfig"

    def getConfigNames(self, ledger_id):
        return self._getEndpoint(Endpoints.CONFIG, ledger_id)

    def getConfigValue(self, ledger_id, config_name):
        config_name = f"{ledger_id}|{config_name}"
        return self._getEndpoint(Endpoints.CONFIG, ledger_id, config_name)
