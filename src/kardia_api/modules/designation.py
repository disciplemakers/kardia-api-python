from enum import Enum
from ..objects.designation_objects import FundingTarget, AdminFee, Receipting, ReceiptingAccount
from requests.models import Response
from .api_module import APIModule

# ------------------------------------ Endpoints ------------------------------------

class Endpoints(Enum):
    PARENT = None
    FUNDS = "Funds"
    FUNDING_TARGETS = "FundingTargets"
    ADMIN_FEES = "AdminFees"
    RECEIPTINGS = "Receiptings"
    RECEIPTING_ACCOUNTS = "ReceiptingAccounts"

class FundEndpoints(Enum):
    PARENT = Endpoints.FUNDS
    FUNDING_TARGETS = "FundingTargets"
    ADMIN_FEES = "AdminFees"
    RECEIPTING = "Receipting"
    RECEIPTING_ACCOUNTS = "ReceiptingAccounts"


# ------------------------------------ Module ------------------------------------

class Designation(APIModule):

    # ------------------------------------ Init ------------------------------------

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()


    def getFundFundingTargets(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.FUNDING_TARGETS, full_fund_id)

    def getFundingTargets(self) -> Response:
        return self._getEndpoint(Endpoints.FUNDING_TARGETS)

    def getFundingTarget(self, ledger_id: str, fund_id: str, target_id: str) -> Response:
        full_target_id = "{}|{}|{}".format(ledger_id, fund_id, target_id)
        return self._getEndpoint(Endpoints.FUNDING_TARGETS, full_target_id)

    def getNextFundingTargetID(self, ledger_id: str, fund_id: str) -> Response:
        next_target_id = 1
        funding_targets = self.getFundFundingTargets(ledger_id, fund_id).json()
        for target in funding_targets:
            if "|" in target:
                target_parts = target.split("|")
                target_id = int(target_parts[2])
                if target_id >= next_target_id:
                    next_target_id = target_id + 1
        return next_target_id

    def createFundingTarget(self, funding_target: FundingTarget):
        # If missing target id, generate
        if not funding_target.a_target_id:
            funding_target.a_target_id = self.getNextFundingTargetID(funding_target.a_ledger_number, funding_target.a_fund)
        self.payload = funding_target.serialize()
        return self._postEndpoint(Endpoints.FUNDING_TARGETS)


    def getFundAdminFees(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.ADMIN_FEES, full_fund_id)

    def getAdminFees(self) -> Response:
        return self._getEndpoint(Endpoints.ADMIN_FEES)

    def getAdminFee(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(Endpoints.ADMIN_FEES, full_fund_id)

    def createAdminFee(self, admin_fee: AdminFee):
        self.payload = admin_fee.serialize()
        return self._postEndpoint(Endpoints.ADMIN_FEES)


    def getFundReceiptings(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.RECEIPTING, full_fund_id)

    def getReceiptings(self) -> Response:
        return self._getEndpoint(Endpoints.RECEIPTINGS)

    def getReceipting(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(Endpoints.RECEIPTINGS, full_fund_id)

    def createReceipting(self, receipting: Receipting):
        self.payload = receipting.serialize()
        return self._postEndpoint(Endpoints.RECEIPTINGS)


    def getFundReceiptingAccounts(self, ledger_id: str, fund_id: str) -> Response:
        full_fund_id = "{}|{}".format(fund_id, ledger_id)
        return self._getEndpoint(FundEndpoints.RECEIPTING_ACCOUNTS, full_fund_id)

    def getReceiptingAccounts(self) -> Response:
        return self._getEndpoint(Endpoints.RECEIPTING_ACCOUNTS)

    def getReceiptingAccount(self, ledger_id: str, fund_id: str, account_id: str) -> Response:
        full_account_id = "{}|{}|{}".format(fund_id, ledger_id, account_id)
        return self._getEndpoint(Endpoints.RECEIPTING_ACCOUNTS, full_account_id)

    def createReceiptingAccount(self, receipting_account: ReceiptingAccount):
        self.payload = receipting_account.serialize()
        return self._postEndpoint(Endpoints.RECEIPTING_ACCOUNTS)
