from datetime import datetime
from enum import Enum
from .acctconfig import AccountConfig
from ..objects.gl_objects import Batch, Transaction
from typing import List
from .gl import GL, PeriodEndpoints
from requests.models import Response
from ..objects.disb_objects import LineItem
from decimal import Decimal


class BatchEndpoints(Enum):
    PARENT = PeriodEndpoints.BATCHES
    LINE_ITEMS = "LineItems"


class Disbursements(GL):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def getLineItem(
        self, ledger: str, year: str, month: str, batch: int, check: int, line_item: int
    ) -> Response:
        period = f"{year}|{ledger}/{month}|{ledger}"
        line_item = f"{ledger}|{batch}|{check}|{line_item}"
        batch = f"{batch}|{ledger}"
        return self._getEndpoint(
            BatchEndpoints.LINE_ITEMS, ledger, period, batch, line_item
        )

    def getLineItems(self, ledger: str, year: str, month: str, batch: int) -> Response:
        period = f"{year}|{ledger}/{month}|{ledger}"
        batch = f"{batch}|{ledger}"
        return self._getEndpoint(BatchEndpoints.LINE_ITEMS, ledger, period, batch)

    def createLineItem(self, year: str, line_item: LineItem) -> Response:
        self.payload = line_item.serialize()
        period = f"{year}|{line_item.a_ledger_number}/{line_item.a_period}|{line_item.a_ledger_number}"
        batch = f"{line_item.a_batch_number}|{line_item.a_ledger_number}"
        return self._postEndpoint(
            BatchEndpoints.LINE_ITEMS, line_item.a_ledger_number, period, batch
        )

    def createLineItems(self, year: str, line_items: List[LineItem]) -> Response:
        res = []
        for line_item in line_items:
            res.append(self.createLineItem(year, line_item))
        return res

    def createBatchWithLineItems(
        self,
        ledger: str,
        year: str,
        month: str,
        line_items: List[LineItem],
        desc: str = "",
        date: datetime = None,
        post: bool = False,
        gl_post: bool = False,
    ) -> dict:
        """Creates a disbursment batch with given line items and will create a journal entry with transactions if gl_post is true.

        Args:
            ledger (str): ledger to create the batch in
            year (str): year to create the batch in
            month (str): month to create the batch in
            line_items (List[LineItem]): line items that will be created for the batch
            desc (str, optional): batch discription. Defaults to "".
            date (datetime, optional): date of the batch. Defaults to None.
            post (bool, optional): post to disbursment ledger? Defaults to False.
            gl_post (bool, optional): post to the general ledger? Defaults to False.

        Raises:
            Exception: If createing the batch object fails
            Exception: If createing a line item object fails

        Returns:
            dict: Dictionary of the batch, line items, and transactions responses.\n\tkeys: batch, line_items, transactions.
        """
        res = {"batch": None, "line_items": [], "transactions": []}
        # If date input is None, set to current date & time
        if not date:
            date = datetime.now()
        # Create batch
        batch = Batch(ledger, month, desc=desc, origin="CD", date=date)
        batch_res = self.createBatch(year, batch)
        if batch_res.status_code != 201:
            raise Exception(
                "Failed to create new batch: \n{}".format(batch_res.content)
            )
        res["batch"] = batch_res
        batch_id = batch_res.json()["a_batch_number"]["v"]
        fund_totals = {}
        account_totals = {}
        trans = []
        tran_attrs = {
            "date": date,
            "isPosted": gl_post.real,
        }
        # Create disbursement batch line items
        for count, li in enumerate(line_items):
            li.a_ledger_number = ledger
            li.a_period = month
            li.a_batch_number = batch_id
            li.a_disbursement_id = 1
            li.a_line_item = count + 1
            li.a_effective_date = date
            li.a_posted = post.real
            li.a_posted_to_gl = gl_post.real
            li.a_batch_number = batch_id
            li_res = self.createLineItem(year, li)
            if li_res.status_code != 201:
                raise Exception(
                    f"Error creating line item {count + 1}\n{li_res.content}"
                )
            res["line_items"].append(li_res)
            if gl_post:
                # amount = f"{li.a_amount['wholepart']}.{li.a_amount['fractionpart']}"
                tran_attrs.update(
                    {
                        "fund": li.a_fund,
                        "account_code": li.a_account_code,
                        "amount": li.a_amount,
                        "comment": li.a_comment,
                    }
                )
                trans.append(Transaction(**tran_attrs))
                if li.a_cash_account_code in account_totals:
                    account_totals[li.a_cash_account_code][0] += li.a_amount
                else:
                    account_totals[li.a_cash_account_code] = [
                        li.a_amount,
                        li.a_comment,
                    ]
                if li.a_fund in fund_totals:
                    fund_totals[li.a_fund][0] += li.a_amount
                else:
                    fund_totals[li.a_fund] = [
                        li.a_amount,
                    ]
        acctconfig = AccountConfig(self.kardia_url, self.user, self.pw)
        config_res = acctconfig.getConfigValue(ledger, "DisbCashFund")
        if config_res.status_code != 200:
            pass  # Handle fail
        disb_cash_fund = config_res.json()
        config_res = acctconfig.getConfigValue(ledger, "GLIfaAcct")
        if config_res.status_code != 200:
            pass  # Handle fail
        interfund_acct = config_res.json()
        if gl_post:
            tran_attrs["fund"] = disb_cash_fund["a_config_value"]
            # Create account balancing entries
            cash_fund_total = Decimal(0)
            for account, val in account_totals.items():
                tran_attrs["account_code"] = account
                tran_attrs["amount"] = val[0] * -1
                tran_attrs["comment"] = "Some useful comment"
                tran_attrs[
                    "account_category"
                ] = Transaction.AccountCategories.CURRENT_ASSET
                trans.append(Transaction(**tran_attrs))
                cash_fund_total += val[0]
            # Create CashFund entry
            tran_attrs[
                "account_category"
            ] = Transaction.AccountCategories.INTERFUND_ASSET
            tran_attrs["account_code"] = interfund_acct["a_config_value"]
            tran_attrs["amount"] = cash_fund_total
            trans.append(Transaction(**tran_attrs))
            # Create Fund balancing entries
            for fund, val in fund_totals.items():
                tran_attrs["fund"] = fund
                tran_attrs["amount"] = val[0] * -1
                tran_attrs["comment"] = "Some useful comment"
                trans.append(Transaction(**tran_attrs))
            res["transactions"] = self.createJEWithTrans(
                ledger, year, month, trans, batch_id=batch_id, desc=desc
            )
        return res
