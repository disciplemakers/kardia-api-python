from enum import Enum
from ..objects.partner_objects import Partner, Address, ContactInfo, Staff
from requests.models import Response
import requests
from .api_module import APIModule

# ------------------------------------ Endpoints ------------------------------------


class Endpoints(Enum):
    PARENT = None
    PARTNERS = "Partners"
    DONORS = "Donors"
    STAFF = "Staff"
    STAFF_LOGINS = "StaffLogins"
    CONTACT_TYPES = "ContactTypes"
    TEST = "Test"
    NEXT_PARTNER_KEY = "NextPartnerKey"


class PartnerEndpoints(Enum):
    PARENT = Endpoints.PARTNERS
    ADDRESSES = "Addresses"
    CONTACT_INFO = "ContactInfo"
    SUBSCRIPTIONS = "Subscriptions"

# ------------------------------------ Module ------------------------------------


class Partner(APIModule):

    # ------------------------------------ Init ------------------------------------

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

    # ------------------------------------ Getters ------------------------------------

    def getPartners(self) -> Response:
        return self._getEndpoint(Endpoints.PARTNERS)

    def getPartner(self, partner_id: str) -> Response:
        return self._getEndpoint(Endpoints.PARTNERS, partner_id)

    def createPartner(self, partner: Partner):
        responses = { 'Partner': None,
                      'Staff': None,
                      'Addresses': [],
                      'ContactInfos': []
                    }
        # If missing partner key, generate
        if not partner.p_partner_key:
            res = self.getNextPartnerKey()
            responses['NextPartnerKey'] = res
            if res.status_code == requests.codes.ok:
                partner.p_partner_key = res.json()['partner_id']
            else:
                return responses
        # Create basic partner record
        self.payload = partner.serialize()
        res = self._postEndpoint(Endpoints.PARTNERS)
        responses['Partner'] = res
        # If staff object exists, create staff record
        if partner.staff_object:
            # If missing partner key, use one from partner object
            if not partner.staff_object.p_partner_key:
                partner.staff_object.p_partner_key = partner.p_partner_key
            res = self.createStaff(partner.staff_object)
            responses['Staff'] = res
        # Addresses
        if partner.address_objects:
            for address in partner.address_objects:
                # If missing partner key, use one from partner object
                if not address.p_partner_key:
                    address.p_partner_key = partner.p_partner_key
                res = self.createPartnerAddress(address)
                responses['Addresses'].append(res)
        # Contact Infos
        if partner.contact_info_objects:
            for contact_info in partner.contact_info_objects:
                # If missing partner key, use one from partner object
                if not contact_info.p_partner_key:
                    contact_info.p_partner_key = partner.p_partner_key
                res = self.createPartnerContactInfo(contact_info)
                responses['ContactInfos'].append(res)
        # Return all Response objects to caller
        return responses

    def getPartnerAddresses(self, partner_id: str) -> Response:
        return self._getEndpoint(PartnerEndpoints.ADDRESSES, partner_id)

    def getPartnerAddress(self, partner_id: str, location_id: str) -> Response:
        address_name = "{}|{}|0".format(partner_id, location_id)
        return self._getEndpoint(PartnerEndpoints.ADDRESSES, partner_id, address_name)

    def getNextPartnerAddressID(self, partner_id: str) -> Response:
        next_address_id = 1
        addresses = self.getPartnerAddresses(partner_id).json()
        for address in addresses:
            if "|" in address:
                address_parts = address.split("|")
                address_id = int(address_parts[1])
                if address_id >= next_address_id:
                    next_address_id = address_id + 1
        return next_address_id

    def createPartnerAddress(self, address: Address):
        # If missing location id, generate
        if not address.p_location_id:
            address.p_location_id = self.getNextPartnerAddressID(address.p_partner_key)
        self.payload = address.serialize()
        return self._postEndpoint(PartnerEndpoints.ADDRESSES, address.p_partner_key)

    def getPartnerContactInfos(self, partner_id: str) -> Response:
        return self._getEndpoint(PartnerEndpoints.CONTACT_INFO, partner_id)

    def getPartnerContactInfo(self, partner_id: str, contact_id: str) -> Response:
        info_name = "{}|{}".format(partner_id, contact_id)
        return self._getEndpoint(PartnerEndpoints.CONTACT_INFO, partner_id, info_name)

    def getNextPartnerContactInfoID(self, partner_id: str) -> Response:
        next_contact_id = 1
        contact_infos = self.getPartnerContactInfos(partner_id).json()
        for contact_info in contact_infos:
            if "|" in contact_info:
                contact_info_parts = contact_info.split("|")
                contact_id = int(contact_info_parts[1])
                if contact_id >= next_contact_id:
                    next_contact_id = contact_id + 1
        return next_contact_id

    def createPartnerContactInfo(self, contact_info: ContactInfo):
        # If missing contact id, generate
        if not contact_info.p_contact_id:
            contact_info.p_contact_id = self.getNextPartnerContactInfoID(contact_info.p_partner_key)
        self.payload = contact_info.serialize()
        return self._postEndpoint(PartnerEndpoints.CONTACT_INFO, contact_info.p_partner_key)

    def getPartnerSubscriptions(self, partner_id: str) -> Response:
        return self._getEndpoint(PartnerEndpoints.SUBSCRIPTIONS, partner_id)

    def getPartnerSubscription(self, partner_id: str, list_id) -> Response:
        # TODO Figure out what the third number means
        list_name = "{}|{}|1".format(list_id, partner_id)
        return self._getEndpoint(PartnerEndpoints.SUBSCRIPTIONS, partner_id, list_name)

    def getStaff(self) -> Response:
        return self._getEndpoint(Endpoints.STAFF)

    def createStaff(self, staff: Staff):
        self.payload = staff.serialize()
        return self._postEndpoint(Endpoints.STAFF)

    def getStaffMember(self, staff_id: str) -> Response:
        return self._getEndpoint(Endpoints.STAFF, staff_id)

    def getStaffLogins(self) -> Response:
        return self._getEndpoint(Endpoints.STAFF_LOGINS)

    def getStaffLogin(self, username) -> Response:
        return self._getEndpoint(Endpoints.STAFF_LOGINS, username)

    def getContactTypes(self) -> Response:
        return self._getEndpoint(Endpoints.CONTACT_TYPES)

    def getContactType(self, contact_type) -> Response:
        return self._getEndpoint(Endpoints.CONTACT_TYPES, contact_type)

    def getTests(self) -> Response:
        return self._getEndpoint(Endpoints.TEST)

    def getTest(self, test) -> Response:
        return self._getEndpoint(Endpoints.TEST, test)

    def getNextPartnerKey(self) -> Response:
        return self._getEndpoint(Endpoints.NEXT_PARTNER_KEY, attrs=True)
