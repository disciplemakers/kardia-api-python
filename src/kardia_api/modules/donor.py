from enum import Enum
from ..objects.donor_objects import Settings
from requests.models import Response
from .api_module import APIModule


class Root(Enum):
    PARENT = None
    DONOR_ID = "/"


class Endpoints(Enum):
    PARENT = Root.DONOR_ID
    GIFTS = "Gifts"
    YEARS = "Years"
    FUNDS = "Funds"
    GIVING_INFO = "GivingInfo"
    DONOR_INFO = "DonorInfo"
    SETTINGS = "Settings"


class GiftEndpoints(Enum):
    PARENT = Endpoints.GIFTS
    RECEIPT = "Receipt"


class YearEndpoints(Enum):
    PARENT = Endpoints.YEARS
    GIFTS = "Gifts"
    FUNDS = "Funds"


class YearGiftEndpoints(Enum):
    PARENT = YearEndpoints.GIFTS
    RECEIPT = "Receipt"


class YearFundEndpoints(Enum):
    PARENT = YearEndpoints.FUNDS
    GIFTS = "Gifts"


class YearFundGiftEndpoints(Enum):
    PARENT = YearFundEndpoints.GIFTS
    RECEIPT = "Receipt"


class FundEndpoints(Enum):
    PARENT = Endpoints.FUNDS
    GIFTS = "Gifts"
    YEARS = "Years"


class FundGiftEndpoints(Enum):
    PARENT = FundEndpoints.GIFTS
    RECEIPT = "Receipt"


class FundYearEndpoints(Enum):
    PARENT = FundEndpoints.YEARS
    GIFTS = "Gifts"


class FundYearGiftEndpoints(Enum):
    PARENT = None
    RECEIPT = "Receipt"


class Donor(APIModule):

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

    def getDonor(self, partner_id: str) -> Response:
        return self._getEndpoint(None, partner_id)

    def getDonors(self):
        return self._getEndpoint(None)

    def getDonorGift(self, partner_id: str, gift_number: int):
        # TODO Figure out how to post a gift
        pass

    def getDonorGifts(self, partner_id: str) -> Response:
        return self._getEndpoint(Endpoints.GIFTS, partner_id)

    def getGiftReceipt(self, partner_id: str, gift_number: int) -> Response:
        return self._getEndpoint(GiftEndpoints.RECEIPT, partner_id, gift_number, attrs=True)

    def getYear(self, partner_id: str, year_id: str) -> Response:
        return self._getEndpoint(Endpoints.YEARS, partner_id, year_id)

    def getYears(self, partner_id: str) -> Response:
        return self._getEndpoint(Endpoints.YEARS, partner_id)

    def getYearGift(self, partner_id: str, year_id: str, gift_number: int) -> Response:
        return self._getEndpoint(YearEndpoints.GIFTS, partner_id, year_id, gift_number)

    def getYearGifts(self, partner_id: str, year_id: str) -> Response:
        return self._getEndpoint(YearEndpoints.GIFTS, partner_id, year_id)

    def getYearGiftReceipt(self, partner_id: str, year_id: str, gift_number: int) -> Response:
        return self._getEndpoint(YearGiftEndpoints.RECEIPT, partner_id, year_id, gift_number)

    def getYearFund(self, partner_id: str, year_id: str, fund_id: str) -> Response:
        return self._getEndpoint(YearEndpoints.FUNDS, partner_id, year_id, fund_id)

    def getYearFunds(self, partner_id: str, year_id: str) -> Response:
        return self._getEndpoint(YearEndpoints.FUNDS, partner_id, year_id)

    def getYearFundGift(self, partner_id: str, year_id: str, fund_id: str, gift_number: int) -> Response:
        return self._getEndpoint(YearFundEndpoints.GIFTS, partner_id, year_id, fund_id, gift_number)

    def getYearFundGifts(self, partner_id: str, year_id: str, fund_id: str) -> Response:
        return self._getEndpoint(YearFundEndpoints.GIFTS, partner_id, year_id, fund_id)

    def getYearFundGiftReceipt(self, partner_id: str, year_id: str, fund_id: str, gift_number: int) -> Response:
        return self._getEndpoint(YearFundGiftEndpoints.RECEIPT, partner_id, year_id, fund_id, gift_number)

    def getFund(self, partner_id: str, fund_id: str) -> Response:
        return self._getEndpoint(Endpoints.FUNDS, partner_id, fund_id)

    def getFunds(self, partner_id: str) -> Response:
        return self._getEndpoint(Endpoints.FUNDS, partner_id)

    def getFundGift(self, partner_id: str, fund_id: str, gift_number: int) -> Response:
        return self._getEndpoint(FundEndpoints.GIFTS, partner_id, fund_id, gift_number)

    def getFundGifts(self, partner_id: str, fund_id: str) -> Response:
        return self._getEndpoint(FundEndpoints.GIFTS, partner_id, fund_id)

    def getFundGiftReceipt(self, partner_id: str, fund_id: str, gift_number: int) -> Response:
        return self._getEndpoint(FundGiftEndpoints.RECEIPT, partner_id, fund_id, gift_number)

    def getFundYear(self, partner_id: str, fund_id: str, year_id: str) -> Response:
        return self._getEndpoint(FundEndpoints.YEARS, partner_id, fund_id, year_id)

    def getFundYears(self, partner_id: str, fund_id: str) -> Response:
        return self._getEndpoint(FundEndpoints.YEARS, partner_id, fund_id)

    def getFundYearGift(self, partner_id: str, fund_id: str, year_id: str, gift_number: int) -> Response:
        return self._getEndpoint(FundYearEndpoints.GIFTS, partner_id, fund_id, year_id, gift_number)

    def getFundYearGifts(self, partner_id: str, fund_id: str, year_id: str) -> Response:
        return self._getEndpoint(FundYearEndpoints.GIFTS, partner_id, fund_id, year_id)

    def getFundYearGiftReceipt(self, partner_id: str, fund_id: str, year_id: str, gift_number: int) -> Response:
        return self._getEndpoint(FundYearGiftEndpoints.RECEIPT, partner_id, fund_id, year_id, gift_number)

    def getGivingInfo(self, partner_id: str, year_id: str) -> Response:
        return self._getEndpoint(Endpoints.GIVING_INFO, partner_id, year_id)

    def getDonorInfo(self, partner_id: str, year_id: str) -> Response:
        return self._getEndpoint(Endpoints.DONOR_INFO, partner_id, year_id)

    def getSettings(self) -> Response:
        return self._getEndpoint(Endpoints.SETTINGS)

    def getSetting(self, donor_id: str, ledger_id: str) -> Response:
        donor_and_ledger_id = "{}|{}".format(donor_id, ledger_id)
        return self._getEndpoint(Endpoints.SETTINGS, donor_and_ledger_id)

    def createSettings(self, settings: Settings):
        self.payload = settings.serialize()
        return self._postEndpoint(Endpoints.SETTINGS, settings.p_partner_key)

