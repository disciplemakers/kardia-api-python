from enum import Enum
from .api_module import APIModule


class SysConfigRoot(Enum):
    PARENT = None
    LEDGER_ID = "/"  # TODO move this to api_module.py


class SysConfig(APIModule):

    def __init__(self, kardia_url, user, pw) -> None:
        super().__init__(kardia_url, user, pw)
        self.module = "config"

    def getConfig(self, ledger):
        return self._getEndpoint(SysConfigRoot.LEDGER_ID, ledger)
