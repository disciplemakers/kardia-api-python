from requests.models import Response
from .api_module import APIModule


class Files(APIModule):

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)

    def getFile(self, filepath: str) -> Response:
        return self.get(endpoint=f"/files/{filepath}", use_kardia_url=True)
