from enum import Enum
from ..objects.fundmanager_objects import Settings
from requests.models import Response
import requests
from .api_module import APIModule

# ------------------------------------ Endpoints ------------------------------------


class Default(Enum):
    PARENT = None
    MANAGER_ID = "/"


class Endpoints(Enum):
    PARENT = Default.MANAGER_ID
    SETTINGS = "Settings"
    FUNDS = "Funds"


# ------------------------------------ Module ------------------------------------


class FundManager(APIModule):
    # ------------------------------------ Init ------------------------------------

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

    # ------------------------------------ Getters ------------------------------------

    def getSettings(self, manager_id: str) -> Response:
        return self._getEndpoint(Endpoints.SETTINGS, manager_id)

    def getSetting(self, ledger_id: str, fund_id: str, manager_id: str) -> Response:
        full_manager_id = "{}|{}|{}".format(ledger_id, fund_id, manager_id)
        return self._getEndpoint(Endpoints.SETTINGS, manager_id, full_manager_id)

    def createSettings(self, settings: Settings):
        self.payload = settings.serialize()
        return self._postEndpoint(Endpoints.SETTINGS, settings.p_staff_partner_key)

    def getFundManagers(self) -> Response:
        """Gets all partners who are staff that manage at least one fund

        Returns:
            requests.models.Response: Response from the api
        """
        return self._getEndpoint(None)

    def getFundManager(self, manager_id: str) -> Response:
        """Get a specific fund manager by their partner id

        Args:
            manager_id (str): partner id for the manager

        Returns:
            requests.models.Response: Response form the api
        """
        return self._getEndpoint(None, manager_id)

    def getFunds(self, manager_id: str) -> Response:
        """Get all the managed funes for a manager

        Args:
            manager_id (str): partner id for the manager

        Returns:
            requests.models.Response: Response form the api
        """
        return self._getEndpoint(Endpoints.FUNDS, manager_id)
