from requests.models import Response
from ..objects.report_objects import SchedReportStatus, SchedReportBatchStatus
from .api_module import APIModule
from enum import Enum
from typing import Dict

# ------------------------------------ Endpoints ------------------------------------


class Endpoints(Enum):
    PARENT = None
    SCHED_REPORTS_TO_BE_SENT = "ScheduledReportsToBeSent"
    SCHED_REPORT_BATCHES = "ScheduledReportBatches"


class SchedReportsEndpoints(Enum):
    PARENT = Endpoints.SCHED_REPORTS_TO_BE_SENT
    PARAMS = "Params"

# ------------------------------------ Module ------------------------------------


class Report(APIModule):

    # ------------------------------------ Init ------------------------------------

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

    # ------------------------------------ Getters ------------------------------------

    def getReport(self, report_file: str, report_params: Dict[str, str]) -> Response:
        return self.get(endpoint=f"/modules/{report_file}", res_format="both", use_kardia_url=True, addl_params=report_params)

    def getSchedReportsToBeSent(self) -> Response:
        return self._getEndpoint(Endpoints.SCHED_REPORTS_TO_BE_SENT)

    def getSchedReportParams(self, sched_report_name) -> Response:
        return self._getEndpoint(SchedReportsEndpoints.PARAMS, sched_report_name)

    def updateSchedReportStatus(self, sched_report_name, sched_report_status: SchedReportStatus) -> Response:
        self.payload = sched_report_status.serialize()
        return self._patchEndpoint(Endpoints.SCHED_REPORTS_TO_BE_SENT, sched_report_name)

    def getSchedReportBatches(self) -> Response:
        return self._getEndpoint(Endpoints.SCHED_REPORT_BATCHES)

    def updateSchedReportBatchStatus(self, sched_report_batch_name,
        sched_report_batch_status: SchedReportBatchStatus) -> Response:
        self.payload = sched_report_batch_status.serialize()
        return self._patchEndpoint(Endpoints.SCHED_REPORT_BATCHES, sched_report_batch_name)
