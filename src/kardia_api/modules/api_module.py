from enum import Enum
import requests
from requests.models import Response
akey_endpoint = "/?cx__mode=appinit&cx__groupname=GRP&cx__appname=KARDIA"


class APIModule:

    def __init__(self, kardia_url, user, pw):
        self.kardia_url = kardia_url
        self.api_url = kardia_url + "/api"
        self.user = user
        self.pw = pw
        self.auth = (self.user, self.pw)
        self.module = ""
        self.reset()
        self.session = requests.Session()
        self.akey = None
        # self.setAkey()
        self.get()

    def setParams(self, mode=None, res_type=None, res_format=None, res_attrs=None, res_levels=None):
        if mode:
            self.mode = mode
        if res_type:
            self.res_type = res_type
        if res_format:
            self.res_format = res_format
        if res_attrs:
            self.res_attrs = res_attrs
        if res_levels and res_levels > 1:
            self.res_levels = res_levels
        return self

    @property 
    def collection(self):
        self.res_type = "collection"
        return self
    
    @property
    def element(self):
        self.res_type = "element"
        self.res_format="attrs"
        return self

    @property
    def both(self):
        self.res_type = "both"
        return self

    def resetParams(self):
        self.mode = "rest"
        self.res_type = None
        self.res_format = None
        self.res_attrs = None
        self.res_levels = None

    def reset(self):
        self.endpoint = ""
        self.method = ""
        self.payload = None
        self.resetParams()

    @property
    def params(self):
        params = {
            "cx__mode": self.mode
        }
        if self.res_type:
            params["cx__res_type"] = self.res_type
        if self.res_format:
            params["cx__res_format"] = self.res_format
        if self.res_attrs:
            params["cx__res_attrs"] = self.res_attrs
        if self.res_levels and self.res_levels > 1:
            params["cx__res_levels"] = self.res_levels
        if (self.method == "post" or self.method == "patch") and self.akey:
            params["cx__akey"] = self.akey
        return params

    def request(self, method="", endpoint=None, payload=None, mode=None, res_type=None, res_attrs=None, res_levels=None, res_format=None, use_kardia_url=False, addl_params={}, isRetry=False):
        self.method = method.lower() if method else self.method
        if endpoint:
            self.endpoint = endpoint
        if payload:
            self.payload = payload
        self.setParams(mode, res_type, res_format, res_attrs)
        url_base = self.kardia_url if use_kardia_url else self.api_url
        url = "{}{}".format(url_base, self.endpoint)
        params = {**self.params, **addl_params}
        if self.method == "get":
            res = self.session.get(url, auth=self.auth, params=params)
        elif self.method == "post":
            if not self.akey:
                self.setAkey()
            res = self.session.post(url, auth=self.auth,
                                    params=self.params, json=self.payload)
        elif self.method == "patch":
            if not self.akey:
                self.setAkey()
            res = self.session.patch(url, auth=self.auth,
                                     params=self.params, json=self.payload)
        elif self.method == "put":
            # TODO may need to check for akey
            res = self.session.put(url, auth=self.auth,
                                   params=self.params, json=self.payload)
        if res.status_code == 401 and not isRetry:
            # Handled refreshing session
            return self.request(method, endpoint, payload, mode, res_type, res_attrs, res_levels, res_format, use_kardia_url, addl_params, isRetry=True)
        else:
            self.reset()
            return res

    def get(self, endpoint=None, payload=None, mode=None, res_type=None, res_format=None, res_attrs=None, res_levels=None, use_kardia_url=False, addl_params={}):
        return self.request("get", endpoint, payload, mode, res_type, res_attrs, res_format=res_format, use_kardia_url=use_kardia_url, addl_params=addl_params)

    def post(self, endpoint=None, payload=None, mode=None, res_type=None, res_format=None, res_attrs=None, res_levels=None):
        return self.request("post", endpoint, payload, mode, res_type, res_format, res_attrs)

    def patch(self, endpoint=None, payload=None, mode=None, res_type=None, res_format=None, res_attrs=None, res_levels=None):
        return self.request("patch", endpoint, payload, mode, res_type, res_format, res_attrs)

    def put(self, endpoint=None, payload=None, mode=None, res_type=None, res_format=None, res_attrs=None, res_levels=None):
        return self.request("put", endpoint, payload, mode, res_type, res_format, res_attrs)

    def _getNest(self, endpoint: Enum) -> int:
        nest = 0
        while endpoint:
            nest += 1
            endpoint = endpoint.PARENT.value
        return nest

    def _setUpEnpointRequest(self, endpoint: Enum, *values, attrs=False):
        endpoints = []

        # Create list of endpoints by prepending the parent
        while endpoint:
            endpoints.insert(0, endpoint.value)
            endpoint = endpoint.PARENT.value
        self.endpoint = "/{}".format(self.module)

        # Pair each endpoint with its value
        for i, e in enumerate(endpoints):
            value = values[i] if i < len(values) else ""
            self.endpoint = "{}/{}/{}".format(self.endpoint, e, value)

        # Determine if the request is for a collection or element
        if len(endpoints):
            attrs = attrs or len(endpoints) == len(values)
        elif values:
            self.endpoint = "{}/{}".format(self.endpoint, values[0])
            attrs = True

        # Set the correct params if the not yet yet set
        if not self.res_format and not self.res_type:
            if attrs:
                self.res_format = "attrs"
            else:
                self.res_type = "collection"

    def _getEndpoint(self, endpoint: Enum, *values, attrs=False) -> Response:
        self._setUpEnpointRequest(endpoint, *values, attrs=attrs)
        return self.get()

    def _postEndpoint(self, endpoint: Enum, *values, attrs=False):
        self._setUpEnpointRequest(endpoint, *values, attrs=attrs)
        return self.post()

    def _patchEndpoint(self, endpoint: Enum, *values, attrs=False):
        self._setUpEnpointRequest(endpoint, *values, attrs=attrs)
        return self.patch()

    def stash(self):
        self._stash = {
            "res_type": self.res_type,
            "res_format": self.res_format,
            "res_attrs": self.res_attrs,
            "res_levels": self.res_levels,
            "endpoint": self.endpoint,
            "method": self.method,
            "payload": self.payload,
        }

    def unstash(self):
        for key in self._stash:
            setattr(self, key, self._stash[key])

    def setAkey(self):
        # TODO refresh key when it becomes invalid
        self.mode = "rest"
        self.stash()
        self.endpoint = akey_endpoint
        self.akey = self.get().json()["akey"]
        self.unstash()
