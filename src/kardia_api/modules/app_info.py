from requests.models import Response
from .api_module import APIModule


class AppInfo(APIModule):

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)

    def getAppInfo(self) -> Response:
        return self.get(endpoint="/app_info.struct", res_format="attrs", use_kardia_url=True)
