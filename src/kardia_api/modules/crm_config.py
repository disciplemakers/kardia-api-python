from enum import Enum
from ..objects.crm_config_objects import Step, StepCollaborator, StepRequirement, Track
from requests.models import Response
from .api_module import APIModule

# ------------------------------------ Endpoints ------------------------------------


class Endpoints(Enum):
    PARENT = None
    TRACKS = "Tracks"
    TAG_TYPES = "TagTypes"
    CONTACT_HIST_TYPES = "ContactHistTypes"
    COLLABORATOR_TYPES = "CollaboratorTypes"
    TODO_TYPES = "TodoTypes"
    WORKFLOWT_YPES = "WorkflowTypes"
    DATAITEM_TYPES = "DataItemTypes"
    DOCUMENT_TYPES = "DocumentTypes"
    COUNTRIES = "Countries"
    TEXT_EXPANSIONS = "TextExpansions"


class TrackEndpoints(Enum):
    PARENT = Endpoints.TRACKS
    STEPS = "Steps"
    COLLABORATORS = "Collaborators"


class StepEndpoints(Enum):
    PARENT = TrackEndpoints.STEPS
    COLLABORATORS = "Collaborators"
    REQUIREMENTS = "Requirements"


# ------------------------------------ Module ------------------------------------


class CRM_Config(APIModule):

    def __init__(self, kardia_url, user, pw):
        super().__init__(kardia_url, user, pw)
        self.module = self.__class__.__name__.lower()

# ------------------------------------ Getters ------------------------------------

    def getTrack(self, track_name: str) -> Response:
        return self._getEndpoint(Endpoints.TRACKS, track_name)

    def getTracks(self: str) -> Response:
        return self._getEndpoint(Endpoints.TRACKS)

    def getTrackStep(self, track_name: str, step_name: str) -> Response:
        return self._getEndpoint(TrackEndpoints.STEPS, track_name, step_name)

    def getTrackSteps(self, track_name: str) -> Response:
        return self._getEndpoint(TrackEndpoints.STEPS, track_name)

    def getStepCollaborator(self, track_name: str, step_name: str, collab_id: str) -> Response:
        return self._getEndpoint(StepEndpoints.COLLABORATORS, track_name, step_name, collab_id)

    def getStepCollaborators(self, track_name: str, step_name: str) -> Response:
        return self._getEndpoint(StepEndpoints.COLLABORATORS, track_name, step_name)

    def getTrackCollaborator(self, track_name: str, step_name: str) -> Response:
        return self._getEndpoint(TrackEndpoints.STEPS, track_name, step_name)

    def getTrackCollaborators(self, track_name: str) -> Response:
        return self._getEndpoint(TrackEndpoints.STEPS, track_name)

    def getTagType(self, tag_id: int) -> Response:
        return self._getEndpoint(Endpoints.TAG_TYPES, tag_id)

    def getTagTypes(self) -> Response:
        return self._getEndpoint(Endpoints.TAG_TYPES)

    def getContactHistType(self, ch_type_id: int) -> Response:
        return self._getEndpoint(Endpoints.CONTACT_HIST_TYPES, ch_type_id)

    def getContactHistTypes(self) -> Response:
        return self._getEndpoint(Endpoints.CONTACT_HIST_TYPES)

    def getCollaboratorType(self, collab_type_id: int) -> Response:
        return self._getEndpoint(Endpoints.COLLABORATOR_TYPES, collab_type_id)

    def getCollaboratorTypes(self) -> Response:
        return self._getEndpoint(Endpoints.COLLABORATOR_TYPES)

    def getTodoType(self, todo_type_id) -> Response:
        return self._getEndpoint(Endpoints.TODO_TYPES, todo_type_id)

    def getTodoTypes(self) -> Response:
        return self._getEndpoint(Endpoints.TODO_TYPES)

    def getWorkflowType(self, wf_type_id) -> Response:
        return self._getEndpoint(Endpoints.WORKFLOWT_YPES, wf_type_id)

    def getWorkflowTypes(self) -> Response:
        return self._getEndpoint(Endpoints.WORKFLOWT_YPES)

    def getDataItemType(self, di_type_id) -> Response:
        return self._getEndpoint(Endpoints.DATAITEM_TYPES, di_type_id)

    def getDataItemTypes(self) -> Response:
        return self._getEndpoint(Endpoints.DATAITEM_TYPES)

    def getDocumentType(self, doc_type_id) -> Response:
        return self._getEndpoint(Endpoints.DOCUMENT_TYPES, doc_type_id)

    def getDocumentTypes(self) -> Response:
        return self._getEndpoint(Endpoints.DATAITEM_TYPES)

    def getCountry(self, country: str) -> Response:
        return self._getEndpoint(Endpoints.COUNTRIES, country)

    def getCountries(self) -> Response:
        return self._getEndpoint(Endpoints.COUNTRIES)

    def getTextExpansion(self, expansion: str) -> Response:
        return self._getEndpoint(Endpoints.TEXT_EXPANSIONS, expansion)

    def getTextExpansions(self) -> Response:
        return self._getEndpoint(Endpoints.TEXT_EXPANSIONS)

    def createTrack(self, track: Track) -> Response:
        self.payload = track.serialize()
        return self._postEndpoint(Endpoints.TRACKS)

    def createTrackStep(self, track_name: str, step: Step) -> Response:
        self.payload = step.serialize()
        return self._postEndpoint(TrackEndpoints.STEPS, track_name)

    def createStepCollaborator(self, track_name: str, step_name: str, collaborator: StepCollaborator) -> Response:
        self.payload = collaborator.serialize()
        return self._postEndpoint(StepEndpoints.COLLABORATORS, track_name, step_name)

    def createSetRequirement(self, track_name: str, step_name: str, req: StepRequirement) -> Response:
        self.payload = req.serialize()
        return self._postEndpoint(StepEndpoints.REQUIREMENTS, track_name, step_name)
