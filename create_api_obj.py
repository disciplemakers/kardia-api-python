import requests
from bs4 import BeautifulSoup
import re
import inspect
import pymysql
from getpass import getpass

TABLES_URL = "https://www.codn.net/projects/kardia/wiki/index.php/Kardia:NewTableList"
TABLE_URL = "https://www.codn.net/projects/kardia/wiki/index.php/Kardia:NewTables_{}"

TYPE_MAP = {
    "cha": "str",
    "var": "str",
    "dat": "datetime",
    "bit": "bool",
    "int": "int",
    "mon": "str",
    "dec": "str"
}

conn = None


def getTablesList() -> list:
    """
    Returns:
        list: List of Kardia tables from DB or from https://www.codn.net/projects/kardia/wiki/index.php/Kardia:NewTableList
    """
    try:
        with conn.cursor() as cursor:
            cursor.execute("show tables")
            tables = [r[0] for r in cursor.fetchall()]
        return tables
    except:
        res = requests.get(TABLES_URL)
        soup = BeautifulSoup(res.content, "lxml")
        tables = [
            a.string
            for a in soup.find_all("a", attrs={"href": re.compile("Kardia:NewTables_")})
            if not a.get("class")
        ]
    # print(tables)


def getTableColumns(table: str) -> list:
    """
    Args:
        table (str): to get columns for

    Returns:
        list: of columns for the given table
    """
    try:
        with conn.cursor() as cursor:
            cursor.execute("SHOW COLUMN IN %s", (table,))
            cols = cursor.fetchall()
            return [
                {
                    "field": col["Field"],
                    "type": col["Type"],
                    "default": col["Default"],
                }
                for col in cols
            ]
    except:
        res = requests.get(TABLE_URL.format(table))
        soup = BeautifulSoup(res.content, "lxml")
        table = soup.find("table")
        columns = []
        try:
            for tr in table.find_all("tr"):
                tds = tr.find_all("td")
                if tds:
                    column = {
                        "field": tds[0].string.strip(),
                        "type": tds[1].string.strip(),
                        "default": tds[2].string.strip(),
                        "comment": tds[3].string.strip(),
                        "discussion": tds[4].string.strip(),
                    }
                    columns.append(column)
        except AttributeError:
            print("Kardia table not found")
    return columns


def createObjectFile(name: str):
    """Create an API Object file with a base API Object for the file.

    Args:
        name (str): API Object name. ex: gl, disb, parner, report
    """
    name = name.split(".")[0]
    base_obj = "".join(w.capitalize() for w in name.split("_"))

    with open("templates/api_obj.txt") as f:
        txt = f.read()
        txt = txt.format(base_obj)

    try:
        with open(f"src/kardia_api/objects/{name}_objects.py", "x+") as f:
            f.write(txt.replace(" " * 4, "\t"))
    except Exception:
        print(f"src/kardia_api/objects/{name}_objects.py already exists")


def createObjForTable(table: str, name: str, parent: str):
    """Creates a API Object for a Kardia table and places it in a api_object python file.

    Args:
        table (str): Kardia table to create object for. See https://www.codn.net/projects/kardia/wiki/index.php/Kardia:NewTableList
        name (str): Name of the object. ex: Year, Period, Parnter, Transaction
        parent (str): API Object file to place the object in. Will be created if file not found. ex: gl, disb, parner, report
    """
    parent = parent.replace("_objects", "").replace(".py", "").lower()
    try:
        with open(f"src/kardia_api/objects/{parent}_objects.py", "r+") as obj_file:
            obj_file.read()
            with open("templates/api_obj_child.txt") as child_tpl:
                txt = child_tpl.read()
                cols = getTableColumns(table)
                child_obj = name.capitalize()
                base_obj = "".join(w.capitalize() for w in parent.split("_")) + "Object"
                params = ", ".join(
                    f"{c['field']}: {TYPE_MAP[c['type'][:3]]}" for c in cols
                )
                toMoney = lambda c: f"self.getKardiaMoney({c['field']})"
                init = "\n\t\t".join(
                    map(
                        str,
                        [
                            f"self.{c['field']} = {toMoney(c) if 'mon' in c['type'] else c['field']}"
                            for c in cols
                        ],
                    )
                )
                txt = txt.format(child_obj, base_obj, params, init)
                obj_file.write(f"\n\n{txt}".replace(" " * 4, "\t"))

    except FileNotFoundError:
        createObjectFile(parent)
        createObjForTable(table, name, parent)


if __name__ == "__main__":
    functions = [createObjForTable, createObjectFile, getTablesList, getTableColumns]
    menu_items = dict(enumerate(functions, start=1))

    if input("Connect to a Kardia DB? y/N ").lower() == "y":
        host = input("host: ")
        user = input("username: ")
        pw = getpass("password: ")
        try:
            conn = pymysql.connect(
                host=host,
                user=user,
                password=pw,
                db="Kardia_DB",
            )
            print("\033[92mConnected to Kardia_DB\033[00m\n")
        except pymysql.err.OperationalError as e:
            print(e)

    while True:
        try:
            print("What would you like to do? (Ctrl+C to quit)\n\n")
            for k, function in menu_items.items():
                print(
                    f"\033[96m{k}: {function.__name__}\033[00m\n    {function.__doc__}"
                )
            selection = int(input("Please enter you selection: "))
            func = menu_items[selection]
            args = inspect.signature(func).parameters
            user_args = [input(f"{arg}: ") for arg in args]
            print(f"Running {func.__name__}({', '.join(user_args)})")
            print(f"{func(*user_args)}\n")
        except KeyboardInterrupt:
            print()
            exit()
